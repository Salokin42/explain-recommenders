import unittest
import numpy as np
import pandas as pd
from pandas.util.testing import assert_frame_equal

from src.DataHandler import DataHandler
from src.Models.SGDBatchRegressor import SGDBatchRegressor
from src.FeatureHandler import FeatureHandler


class SGDBatchRegressorTest(unittest.TestCase):
    def setUp(self):
        data_handler = DataHandler()
        ratings, self.movie_features, user_features = data_handler.load_ratings_dev(), data_handler.load_movie_features_raw(), data_handler.load_user_features_dev_raw()
        ratings = ratings.iloc[0:100]
        avg_user_rating = user_features['user_avg_rating']
        user_features.drop(labels=['user_avg_rating'], axis=1, inplace=True)
        self.X = ratings.loc[:, ['userId', 'movieId']]
        self.y = ratings['rating']

    def tearDown(self):
        pass

    def test_fit_runs(self):
        clf = SGDBatchRegressor()
        clf.fit(X=self.X, y=self.y, movie_features=self.movie_features)
        print(clf)

    def test_predict_runs(self):
        clf = SGDBatchRegressor()
        clf.fit(X=self.X, y=self.y, movie_features=self.movie_features)
        predictions = clf.predict(self.X)
        print(predictions)
