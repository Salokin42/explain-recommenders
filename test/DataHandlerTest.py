import unittest
import datetime
from pathlib import Path

from DataHandler import DataHandler
import pandas as pd

class DataHandlerTest(unittest.TestCase):
    def setUp(self):
        #self.data_handler = DataHandler('ml-20m')
        self.data_handler = DataHandler('ml-latest-small')

        self.ratings_training = self.data_handler.load_ratings_train()

    def tearDown(self):
        pass

    # def test_save_model_pkl(self):
    #     model = BaselineOnly()
    #     model.fit(self.surprise_trainset)
    #     timestamp = datetime.datetime.now().__str__()
    #     self.result_handler.save_model('testmodel', "28_07", model)
    #
    #     file_path = Path(self.result_handler.MODEL_FOLDER / ("28_07" + "_" + 'testmodel'))
    #     self.assertTrue(file_path.is_file())

    # def test_build_userId_train_index(self):
    #     userId_indexes = self.data_handler._build_userId_train_index()
    #     unique_Users = self.ratings_training['userId'].unique()
    #
    #     self.assertEqual(len(userId_indexes), unique_Users.__len__())
    #
    # def test_load_userId_train_index(self):
    #     userId_indexes_1 = self.data_handler._build_userId_train_index()
    #     userId_indexes_2 = self.data_handler.load_userId_train_index()
    #     # print(id(userId_indexes_1))
    #     # print(id(userId_indexes_2))
    #     self.assertCountEqual(userId_indexes_1, userId_indexes_2)
    #
    # def test_add_ratings(self):
    #     user_ratings = [(611, 1214, 4.0, 964981855),
    #                     (611, 1219, 2.0, 964983393),
    #                     (611, 1220, 5.0, 964981909),
    #                     (611, 1222, 5.0, 964981909),
    #                     (611, 1224, 5.0, 964984018),
    #                     (611, 1226, 5.0, 964983618),
    #                     (611, 1240, 5.0, 964983723),
    #                     (611, 1256, 5.0, 964981442)]
    #     df_user_ratings =  pd.DataFrame(user_ratings, columns=['userId', 'movieId', 'rating', 'timestamp'])
    #     complete_ratings = self.data_handler.add_ratings(self.ratings_training, user_ratings)
    #     complete_ratings.loc[complete_ratings['userId'] == 611]
    #     #self.assertTrue(df_user_ratings.isin(complete_ratings, ignore_index=True).all().all())
    #     self.assertFalse(complete_ratings.loc[complete_ratings['userId'] == 611].empty)



if __name__ == '__main__':
    unittest.main()
