import unittest
import datetime
import numpy as np
from pathlib import Path

from DataHandler import DataHandler
from RecommenderSGDBatch import RecommenderSGDBatch

from ResultHandler import Result, ResultHandler
from surprise_wrapper import *

from surprise.prediction_algorithms import *
from surprise.model_selection import train_test_split


class RecommenderTest(unittest.TestCase):
    def setUp(self):
        self.data_handler = DataHandler('ml-20m')
        self.model = self.data_handler.load_model()
        self.recommender = RecommenderSGDBatch(self.data_handler, self.model)
        self.data_handler = DataHandler()

        self.ratings = self.data_handler.load_ratings_train()
        # self.exp_ratings_raw = '[{"userId": "51223582", "movieId": "356", "rating": "5", "timestamp": 1545234553.7837782}, {"userId": "39809452", "movieId": "296", "rating": "4", "timestamp": 1545234553.7837782}, {"userId": "01129255", "movieId": "589", "rating": "3", "timestamp": 1545234553.7837782}, {"userId": "84376236", "movieId": "2858", "rating": "5", "timestamp": 1545234553.7837782}, {"userId": "76659571", "movieId": "4993", "rating": "4", "timestamp": 1545234553.7837782}, {"userId": "12463398", "movieId": "858", "rating": "5", "timestamp": 1545234553.7837782}, {"userId": "15137114", "movieId": "2028", "rating": "2", "timestamp": 1545234553.7837782}]'
        # self.exp_ratings = self.controller.convert_json_ratings_to_df(self.exp_ratings_raw)

    def tearDown(self):
        pass

    # def test_run_get_unseen_item_ids(self):
    #
    #     self.recommender.get_unseen_item_ids(self.exp_ratings)
    #
    # def test_run_predict_all_unseen_itemIds(self):
    #     ratings_trainset = supr_build_ratings_trainset_from_df(
    #         self.data_handler.add_ratings(self.ratings, self.exp_ratings))
    #     self.recommender.algorithm.fit(ratings_trainset)
    #     unseen_items = self.recommender.get_unseen_item_ids(self.exp_ratings)
    #     test = self.recommender.predict_all_unseen_itemIds(99999, unseen_items)
    #
    # def test_run_full_recommend_top_n(self):
    #     predictions = self.recommender.full_recommend_top_n(self.exp_ratings_raw)
    #     print("test")

    def test_filter_seen_movies(self):
        movies = self.data_handler.load_movies()
        self.recommender.X = movies.iloc[0:10]
        movies_filtered = self.recommender._filter_seen_movies(movies)

        self.assertTrue(len(movies) > len(movies_filtered))







if __name__ == '__main__':
    unittest.main()
