import unittest
import numpy as np
import pandas as pd
from pandas.util.testing import assert_frame_equal

from src.DataHandler import DataHandler
from src.Explainer import MovieLensDomainMapper

class MovieLensDomainMapperTest(unittest.TestCase):
    def setUp(self):
        self.movie_lens_domain_mapper = MovieLensDomainMapper()

    def test_map_exp_ids(self):
        input = [(1, 0.99), (5,0.99), (1001, 0.99)]
        exp = self.movie_lens_domain_mapper.map_exp_ids(input)
        valid = [('007', 0.99), ('1930s',0.99), ('swashbuckler', 0.99)]

        self.assertEqual(exp, valid)
