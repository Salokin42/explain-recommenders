import unittest
import pandas as pd

from DataHandler import DataHandler


class DataHandlerTest(unittest.TestCase):
    def setUp(self):
        self.data_handler = DataHandler()

    def tearDown(self):
        pass

    def test_movie_features_raw_shape(self):
        movie_features_shape = self.data_handler.load_movie_features_raw().shape
        self.assertEqual(movie_features_shape, (27278, 1148))

    def test_movie_features_scaled_scaled(self):
        movie_features_shape = self.data_handler.load_movie_features().shape
        self.assertEqual(movie_features_shape, (27278, 1148))

    def test_user_features_raw_shape(self):
        user_features_shape = self.data_handler.load_user_features_raw().shape
        self.assertEqual(user_features_shape, (138493, 1149))

    def test_user_features_scaled_shape(self):
        user_features_shape = self.data_handler.load_user_features().shape
        self.assertEqual(user_features_shape, (138493, 1149))
