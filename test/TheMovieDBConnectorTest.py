import urllib.request
import json

from numpy.f2py.auxfuncs import throw_error

from src.TheMovieDBConnector import TheMovieDBConnector
from src.DataHandler import FeatureDataHandler
from src.FeatureHandler import FeatureHandler

data_handler = FeatureDataHandler()
feature_handler = FeatureHandler()
the_movie_db_connector = TheMovieDBConnector(data_handler)


# !!! für 2 Filme aus den IMDB "top 342" ist keine Filmbeschreibung vorhanden/ zu finden
# descriptions[descriptions == 'Keine Filmbeschreibung vorhanden']

# get 342 movie ids with highest imdb feature
movie_features = data_handler.load_movie_features_subset()
movie_features.columns = FeatureHandler.replace_feature_descriptions(movie_features.columns,data_handler)
movie_features = movie_features[movie_features['imdb top 250'] > 0.9] #top 342
movieIds = movie_features.index
# movieIds = movieIds[0:20]

descriptions = the_movie_db_connector.get_movie_descriptions(movieIds)

# movie_descriptions = the_movie_db_connector.get_movie_descriptions(movieIds)


# ids_dict = the_movie_db_connector.get_tmdbIds(movieIds)
# request_urls_dict = the_movie_db_connector._build_request_urls(ids_dict)
# request_urls_dict = dict(list(request_urls_dict.items())[:20])




# url = request_urls_dict[1]
# url_false = 'https://api.themoviedb.org/3/movie/-999?api_key=8d135f10f44bf6f89989b5e5c489a160&language=de-DE&region=DE'

# response = the_movie_db_connector._get_request_overview(url_false)
# n_items = dict(list(request_urls_dict.items())[:50])
# responses = the_movie_db_connector._get_request_overviews(n_items)

# tmdbId = links.loc[0, 'tmdbId']

## check if response = empty

# url = urls[862]
#
# with urllib.request.urlopen(url) as response:
#     response_json = response.read()
#
# movie_information = json.loads(response_json)
# overview = movie_information['overview']
#
# movie_description = {}
# try:
#     with urllib.request.urlopen(url) as response:
#         response_json = response.read()
#     movie_information = json.loads(response_json)
#
#     movie_description[862] = movie_information['overview']
# except:
#     movie_description[862] = 'No description available'
#
#
#
# Build the api url  manually
# beschreibung ist dann im json object in overview
#
# def build_request_url(tmbId):
#     url = 'https://api.themoviedb.org/3/movie/{tmbId}?api_key={key}&language=de-DE&region=DE'.format(tmbId=tmbId,
#                                                                                                      key=API_KEY)
#     return url
#
#
# url = build_request_url(tmdbId)
#
# with urllib.request.urlopen(url) as response:
#     response_json = response.read()
#
# movie_information = json.loads(response_json)
# overview = movie_information['overview']