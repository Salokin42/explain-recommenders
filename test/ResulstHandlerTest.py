import unittest
import datetime
from pathlib import Path

from DataHandler import DataHandler
from ResultHandler import Result, ResultHandler
from surprise_wrapper import *

from surprise.prediction_algorithms import *
from surprise.model_selection import train_test_split


class ResultHandlerTest(unittest.TestCase):
    def setUp(self):
        self.dataset = 'ml-20m_try'
        self.result_handler = ResultHandler(dataset=self.dataset)
        self.data_handler = DataHandler('ml-20m')
        self.data = self.data_handler.load_ratings_try()
        self.data_snippet = self.data[0:500]

        self.surprise_data = build_surprise_data(self.data_snippet)
        self.surprise_trainset, self.surprise_testset = train_test_split(self.surprise_data)

    def tearDown(self):
        # delete entry from logfile again
        pass

    # def test_save_model(self):
    #     for model_name, model in {'BaselineOnly()': BaselineOnly()}.items():
    #         result = evaluate_model('ml-20m-try', self.surprise_trainset, model, 3)

    # def test_save_model_pkl(self):
    #     model = BaselineOnly()
    #     model.fit(self.surprise_trainset)
    #     timestamp = datetime.datetime.now().__str__()
    #     self.result_handler.save_model('testmodel', "28_07", model)
    #
    #     file_path = Path(self.result_handler.MODEL_FOLDER / ("28_07" + "_" + 'testmodel'))
    #     self.assertTrue(file_path.is_file())

    def test_save_and_read_result(self):
        result = evaluate_model('unit_test', self.surprise_trainset, BaselineOnly(), 2)
        result_pd = result.to_pandas_series()
        self.result_handler.save_result(result)
        read_pd_result = self.result_handler.read_results()


if __name__ == '__main__':
    unittest.main()
