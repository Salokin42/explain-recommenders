import numpy as np
import pandas as pd

from src.DataHandler import DataHandler
from src.FeatureHandler import FeatureHandler
from src.RecommenderSGDBatch import RecommenderSGDBatch
from src.Explainer import InteractionRegressionExplainer

# input > WebInterface
mock_ratings = pd.DataFrame(
    {'userId': [999999] * 6, 'movieId': [1, 3, 5, 11, 16, 2391], 'rating': [1, 1, 1, 5, 5, 5]})

# setup
data_handler = DataHandler()
model = data_handler.load_model('SGDBatchRegressor_subset.joblib')
feature_handler_fitted = model.feature_handler
recommender = RecommenderSGDBatch(data_handler, model)

explainer = InteractionRegressionExplainer()
training_data_representative = InteractionRegressionExplainer.build_training_data_representative(feature_handler_fitted,
                                                                                                 5000, data_handler)
feature_names = FeatureHandler.replace_feature_descriptions(training_data_representative.columns.tolist(), data_handler)

X = mock_ratings.loc[:, ['userId', 'movieId']]
y = mock_ratings.loc[:, 'rating']

# # # # recommend
# recommender.fit(X, y)  # has side effects on models.feature_handler
# recommendations = recommender.recommend_top_n(2, return_seen_movies=False)
# #
# # # #explain
# explainer.fit(feature_handler_fitted, model.predict, training_data_representative, feature_names=feature_names,
#               n_explanation_features=5)
#
#
# explanations = explainer.explain_Xs(recommendations[['userId', 'movieId']])
# explanations['prediction'] = recommendations['prediction']
#
# exp = explanations['explanation'].iloc[0]
# exp.as_pyplot_figure().show()