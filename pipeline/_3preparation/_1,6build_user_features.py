import numpy as np
import os
from DataHandler import DataHandler
from sklearn.preprocessing import StandardScaler

from src.FeatureHandler import FeatureHandler

import logging

logging.basicConfig(level=logging.DEBUG)

data_handler = DataHandler()
feature_handler = FeatureHandler()


def generate_user_features_batch(ratings, movie_features, batch_size=1000):
    path = os.path.join(data_handler.path_processed, 'user_features_raw_temp.csv')

    user_ids = ratings['userId'].unique()

    for pos in range(0, len(user_ids), batch_size):  # iterate through ratings of unique user_id
        logging.info(
            'Generating user_features for Position {pos} from {len} - {per}'.format(pos=pos, len=len(user_ids),
                                                                                    per=(pos / len(user_ids)) * 100))
        cur_user_ids = user_ids[pos:pos + batch_size]
        cur_ratings = ratings[ratings['userId'].isin(cur_user_ids)]
        X = cur_ratings.loc[:, ['userId', 'movieId']]
        y = cur_ratings['rating']

        user_features = FeatureHandler.build_user_features(X, y, movie_features, add_avg_user_rating=True)

        if os.path.isfile(path):
            user_features.to_csv(path, mode='a', header=False)
        else:
            user_features.to_csv(path, header=True)

# Generate User Features raw set
# ratings = data_handler.load_ratings_all()
# movie_features = data_handler.load_movie_features_raw()
# generate_user_features_batch(ratings, movie_features, batch_size=1500)
# Generate user_features_dev  aw
# user_features = data_handler.load_user_features_raw()
# user_ids_ratings_dev = data_handler.load_ratings_dev()['userId'].unique()
# user_features_dev = user_features.loc[user_ids_ratings_dev]
#
# path = os.path.join(data_handler.path_processed, 'user_features_dev_raw.csv')
# user_features_dev.to_csv(path, header=True)
