""" Database implementation"""

import pandas as pd
import os
from sklearn.model_selection import GroupShuffleSplit
from sklearn.model_selection import train_test_split

from DataDatabaseHandler import DataDatabaseHandler

data_db_handler = DataDatabaseHandler("ml-20m")
path_processed = data_db_handler.path_processed
cur = data_db_handler.cur

ratings = data_db_handler.load_ratings_all()
# user_id_counts = ratings['userId'].value_counts().sort_index().to_dict()
unique_user_ids = ratings['userId'].unique()

train_user_ids, test_user_ids = train_test_split(unique_user_ids, test_size=0.2, random_state=42)
dev_user_ids = train_user_ids[0:500]
groups = {'dev': dev_user_ids, 'train': train_user_ids, 'test': test_user_ids}


# build sql statement that multiplies


# for user_id in unique_user_ids:
#     strSql = 'DROP TABLE IF EXISTS %s' % tablename
#     cur.execute(strSql)


""" Split into different tables accoording to group"""
# for groupname, user_ids in groups.items():
#     tablename = 'ratings_meta_' + groupname
#     strSql = 'DROP TABLE IF EXISTS %s' % tablename
#     cur.execute(strSql)
#     strSql = 'CREATE TABLE %s AS SELECT * FROM ratings_metadata WHERE userId IN %s' % (tablename, str(tuple(user_ids)))
#     print(strSql)
#
#     cur.execute(strSql)


# TODO Write to csv file (try to load whole set in memory won't suceed? Than adapt DataDatabase Handler to load in rows

# for groupname, user_ids in groups.items():
#     path = os.path.join(path_processed, 'ratings_meta_' + groupname + '.csv')
#     strSql = 'SELECT * FROM %s' % tablename
#     cur.execute(strSql)
#
