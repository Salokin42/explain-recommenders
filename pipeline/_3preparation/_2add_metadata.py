##################################### runs on csv from here on
# Need to export movies_meta and user features "manually" from SQL DB through dump function

import os
import pandas as pd
from sklearn.model_selection import train_test_split
from DataHandler import DataHandler

data_handler = DataHandler('ml-20m')


def build_split_ratings():
    ratings = data_handler.load_ratings_all()
    # user_id_counts = ratings['userId'].value_counts().sort_index().to_dict()
    unique_user_ids = ratings['userId'].unique()

    train_user_ids, test_user_ids = train_test_split(unique_user_ids, test_size=0.2, random_state=42)
    dev_user_ids = train_user_ids[0:500]
    groups = {'dev': dev_user_ids, 'train': train_user_ids, 'test': test_user_ids}

    for groupname, user_ids in groups.items():
        group_ratings = ratings[ratings['userId'].isin(user_ids)]
        data_handler.save_processed_data_as_csv(group_ratings, 'ratings_{}.csv'.format(groupname))


######### Don't use. Creates files > 150GB
def build_ratings_meta_for_all_ratings():
    files = ['ratings_dev.csv', 'ratings_train.csv', 'ratings_test.csv']
    for file in files:
        build_ratings_meta_(file)


def build_ratings_meta_(file):
    """
    :param file: ratings file
    :return: creates new ratings file in the processed folder with user and movie metada attached
    """
    c_size = 5000
    c_counter = 0

    user_features = data_handler.load_user_features()
    movie_features = data_handler.load_movie_features()

    ratings_saving_path = os.path.join(data_handler.path_processed, 'meta_' + os.path.basename(file))

    for ratings in pd.read_csv(os.path.join(data_handler.path_processed, file), chunksize=c_size):
        c_counter += c_size
        print('Start Processing row: {}'.format(c_counter))
        ratings = ratings.merge(user_features, left_on='userId', right_on='user_userId')
        ratings = ratings.merge(movie_features, on='movieId')

        if os.path.exists(ratings_saving_path):
            append_write = 'a'
            header = False
        else:
            append_write = 'w'
            header = True
        with open(ratings_saving_path, append_write) as f:
            ratings.to_csv(f, header=header, index=False)
    print('Processed finished after {} chunks'.format(c_counter))


