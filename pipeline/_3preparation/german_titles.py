from DataHandler import FeatureDataHandler
import pandas as pd
import os

data_handler = FeatureDataHandler()

titles = data_handler.load_title_translations()
titles = titles[titles['region'] == 'DE']
titles['titleId'] = titles['titleId'].str.slice(2)
titles['titleId'] = pd.to_numeric(titles['titleId'])
titles = titles[~titles.duplicated(subset='titleId')]  # delete all duplicate/ alternative titles
titles.set_index('titleId', inplace=True)
titles = titles['title']
# make sure there are not duplicates

links = data_handler.load_links()
links.set_index('imdbId', inplace=True)

translations = links.join(titles, how='inner')
translations.set_index('movieId', inplace=True)
translations = translations['title']


#movies
movies = data_handler.load_movies()
movies['year'] = movies.title.str.extract("\((\d{4})\)", expand=True)

# movies.year = pd.to_datetime(movies.year, format='%Y')
movies.title = movies.title.str[:-7]

movies = movies.merge(translations.to_frame(), left_index=True, right_index=True, how='left')
movies['title_y'].fillna(movies['title_x'], inplace=True)
movies.drop('title_x', axis=1, inplace=True)
movies.rename({'title_y':'title'},axis=1, inplace=True)
movies.dropna(inplace=True)

path = os.path.join(data_handler.path_processed,'movies_german.csv')
movies.to_csv(path)

#
# df = translations[translations.str.contains('Die Verurteilten')]
# df2 = links[links.imdbId.isin(['0111161', '2457822'])]
#
# titles.sort_values(by='titleId', inplace=True)
# titles = titles[titles.duplicated(subset='titleId', keep=False)]
# titles.index.duplicated().sum()
#
# movies[movies['title'].str.contains('Shawshank Redemption')]
# translations[translations == 'Die Verurteilten']
# movies[movies['title'] == 'Die Verurteilten']





