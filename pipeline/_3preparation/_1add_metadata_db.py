import pandas as pd

from DataDaskHandler import DataDaskHandler
from DataDatabaseHandler import DataDatabaseHandler

data_db_handler = DataDatabaseHandler("ml-20m")
con = data_db_handler.con
cur = data_db_handler.cur


def load_files_into_db():
    ratings = data_db_handler.load_ratings_all()
    movies_metadata = data_db_handler.load_movie_features()

    ratings.to_sql(name='ratings', con=con, if_exists='replace')
    movies_metadata.to_sql(name='movies', con=con, if_exists='replace')


def add_metadata_to_movies():
    movies = data_db_handler.load_movies()
    genome_scores = data_db_handler.load_raw_genome_scores()
    genome_tags = data_db_handler.load_raw_genome_tags()

    # Modify Genres
    # genres_unique = pd.DataFrame(movies.genres.str.split('|').tolist()).stack().unique()
    # genres_unique = pd.DataFrame(genres_unique, columns=['genre'])
    movies = movies.join(movies.genres.str.get_dummies().astype(bool))
    movies.drop('genres', inplace=True, axis=1)

    # Add Genome_Tags
    genome_score_table = genome_scores.pivot(index='movieId', columns='tagId', values='relevance')
    movies = movies.merge(genome_score_table, on='movieId', how='left')

    data_db_handler.save_movies_metadata(movies)
    ## Add Tags > nicht implementiert, da es 38643 verschiedene Tags gibt

    # ratings = ratings.merge(movies, on='movieId')
    #
    # data_handler.save_processed_data(ratings, 'ratings_all_with_genres')


def save_movies_metadata_as_csv():
    strSql = 'SELECT * FROM movies'
    filename = 'movies_metadata'
    data_db_handler.save_as_csv(strSql, filename)


def add_metadata_to_ratings_db():
    # FIXME runs out of space - not useable
    strSql = 'DROP TABLE IF EXISTS ratings_metadata'
    cur.execute(strSql)
    strSql = 'CREATE VIEW view_ratings_metadata AS SELECT * FROM ratings r LEFT JOIN movies m ON r.movieId = m.movieId'
    cur.execute(strSql)
    # CREATE TABLE ratings_metadata AS SELECT * FROM ratings r LEFT JOIN movies m ON r.movieId = m.movieId


def calc_user_features_db():
    strSql = 'SELECT * FROM view_ratings_metadata LIMIT 1'
    cur.execute(strSql)

    # column_names = ['ROUND(' + description[0] + '* ratings,4)' for description in cur.description]

    strSql = 'CREATE VIEW "user_features" AS SELECT '

    for description in cur.description:
        desc = description[0]
        if desc == 'userId':
            strSql += '"' + desc + '"'
        elif desc == 'rating':
            strSql += 'round(' + 'avg("' + desc + '")' + ',4)'
            # add columns to ignore here
        elif desc in ['movieId', 'movieId:1', 'index', 'index:1', 'timestamp', 'avg(movieId:1 ']:
            continue
        else:
            # FIXME Add "AS" statement to give column the name of current desc; change the follwing line than to remove the last ","
            strSql += 'round(avg("{proper_desc}" * "rating"), 4)'.format(proper_desc="".join(desc.split()))

        strSql += ' AS "user_{column_name}", '.format(column_name=desc)

    strSql = strSql[:strSql.rfind(',')]
    strSql += ' FROM view_ratings_metadata GROUP BY "userId"'
    print(strSql)
    cur.execute(strSql)


""" used to rename pandas dataframe"""


def get_column_names_for_ratings_metadata():
    strSql = 'SELECT * FROM view_ratings_metadata LIMIT 1'
    cur.execute(strSql)
    pandas_columns_names = []
    for description in cur.description:
        desc = description[0]
        if desc in ['movieId', 'movieId:1', 'index', 'index:1', 'timestamp', 'avg(movieId:1 ']:
            pass
        else:
            pandas_columns_names.append('user_' + desc)
    return pandas_columns_names


# load_files_into_db()
# add_metadata_to_movies()
# add_metadata_to_ratings()
# calc_user_features_db()
# split_db()
# build_ratings_meta_all()

# user_features = data_db_handler.read_user_features_from_db()
# movie_features = data_db_handler.read_movie_features_from_db()


