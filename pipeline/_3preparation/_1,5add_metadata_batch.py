import numpy as np
import os
from DataHandlerFeatures import DataHandlerFeatures
from sklearn.preprocessing import StandardScaler


import logging
logging.basicConfig(level=logging.DEBUG)

# def build_generator_user_id(data_handler):
#     def build_generator_for_ratings_movie_meta(self, batch_size):
#         for pos in range(0, len(self.ratings), batch_size):
#             cur_ratings = self.ratings.iloc[pos:pos + batch_size]
#             cur_ratings = cur_ratings.merge(self.movie_features, on='movieId')
#
#             yield cur_ratings

#Deprcated, use 1,6build_user_features
def generate_user_features(data_handler):
    batch_gen = data_handler.build_generator_for_ratings_by_user_id_meta_movies(batch_size=1000)
    path = os.path.join(data_handler.path_processed, 'user_features_temp.csv')

    for rating in batch_gen:
        rating = rating.drop(['timestamp', 'index', 'movieId'], axis=1)
        rating.iloc[:, 2:] = rating.iloc[:, 2:].apply(lambda x: x * rating['rating'])
        rating = rating.groupby('userId').mean()
        rating.rename(columns={'rating': 'avg_rating'}, inplace=True)
        rating.rename(columns=lambda x: "user_" + x, inplace=True)

        if os.path.isfile(path):
            rating.to_csv(path, mode='a', header=False)
        else:
            rating.to_csv(path, header=True)


def normalize_features(data_handler_features):
    def normalize_vector(features_vector):
        scaler = StandardScaler()
        return scaler.fit_transform(features_vector)


    logging.info('Starting normalization')
    user_features = data_handler.user_features
    user_features.iloc[:, 1:] = normalize_vector(user_features.iloc[:, 1:])

    path = os.path.join(data_handler.path_processed, 'user_features_norm.csv')
    user_features.to_csv(path, header=True)
    logging.info('Finished user_features - starting movie_features')

    movie_features = data_handler.movie_features
    movie_features.iloc[:, 2:] = normalize_vector(movie_features.iloc[:, 2:])
    path = os.path.join(data_handler.path_processed, 'movie_features_norm.csv')
    movie_features.to_csv(path, header=True)
    logging.info('Finished movie_features')


data_handler = DataHandlerFeatures(ratings_type='all')
#generate_user_features(data_handler)
# normalize_features(data_handler)


user_features = data_handler.load_user_features()
user_ids_ratings_dev = data_handler.load_ratings_dev()['userId'].unique()
user_features_dev = user_features[user_features['userId'].isin(user_ids_ratings_dev)]

path = os.path.join(data_handler.path_processed, 'user_features_dev.csv')
user_features_dev.to_csv(path, header=True, index=False)


# Test data_handler.build_generator_for_ratings_by_user_id_meta_movies
# Test that there are no duplicated of user_ids in the generator
# batch_gen = data_handler.build_generator_for_ratings_by_user_id_meta_movies(batch_size=10)
# seen_user_ids = np.empty(0)
# for ratings_batch in batch_gen:
#     uniqe_userIds = ratings_batch['userId'].unique()
#
#     if np.isin(seen_user_ids,uniqe_userIds).any():
#         print( 'error, userID is in already in set')
#
#     seen_user_ids =np.append(seen_user_ids, uniqe_userIds)
#     print(seen_user_ids)
