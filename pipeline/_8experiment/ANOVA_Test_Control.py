import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from io import StringIO
import sys

import scipy.stats as stats
import researchpy as rp
import statsmodels.api as sm
from statsmodels.formula.api import ols

from src.DataHandler import FeatureDataHandler
from pipeline._8experiment.helper_evaluation import *

data_handler = FeatureDataHandler()

experiment = data_handler.load_experiment_data()
experiment = adjust_experiment_data(experiment)

constructs = constructs_items.keys()

# Perform Anova Test on the constructs


result_list = []
control_variables = ['q_Alter', 'q_Bildung', 'q_Geschlecht',
                     'q_Variante', 'q_Vorerfahrung']

p_values_control_variables = {}

for control_var in control_variables:
    p_values_construct = {}
    for construct in constructs:

        realtionship_to_test = "{konstrukt} ~C({control_var})".format(konstrukt=construct, control_var=control_var)
        results = ols(realtionship_to_test, data=experiment).fit()
        p_values_construct[construct] = results.f_pvalue

    p_values_control_variables[control_var] = p_values_construct


df = pd.DataFrame(p_values_control_variables)


df.drop('q_Variante', inplace=True, axis=1)
df.round(decimals=3).to_excel(os.path.join(data_handler.path_thesis, 'Control Variables ANOVA only p-value.xlsx'))