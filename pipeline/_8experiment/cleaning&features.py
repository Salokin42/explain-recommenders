import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from src.DataHandler import FeatureDataHandler
from src.FeatureHandler import FeatureHandler
from pipeline._8experiment.helper_evaluation import *

data_handler = FeatureDataHandler()
feature_handler = FeatureHandler()

experiment = data_handler.load_experiment_data_complete() # load File from Preprocessing
recommendations = data_handler.load_experiment_recommendations()
ratings = data_handler.load_experiment_ratings()
explanations = data_handler.load_experiment_explanations()
top_user_features = data_handler.load_experiment_top_user_features()

## Cleaning
experiment = experiment[experiment['Dummy'] == 1]
experiment.drop('Dummy',  axis=1, inplace=True)
# experiment_variant_match = experiment[experiment['Variante'] == experiment['q_Variante']]

# Calculate Konstrukte
for construct, items in constructs_items.items():
    experiment[construct] = experiment[items].mean(axis=1)

#0/1 enconding for explanations
experiment['E_LIME'] = ((experiment['Variante'] == 'L') | (experiment['Variante'] == 'LP'))
experiment['E_LIME'] = experiment['E_LIME'].astype(int)
experiment['E_Präf'] = ((experiment['Variante'] == 'P') | (experiment['Variante'] == 'LP'))
experiment['E_Präf'] = experiment['E_Präf'].astype(int)


experiment.to_csv(os.path.join(data_handler.path_evaluation, 'experiment.csv'))

# experiment['Transparenz'] = experiment[['Tra1']].mean(axis=1)
