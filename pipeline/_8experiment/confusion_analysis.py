import pandas as pd
import os
from scipy import stats

from src.DataHandler import FeatureDataHandler
from pipeline._8experiment.helper_evaluation import *

data_handler = FeatureDataHandler()

experiment = data_handler.load_experiment_data()
experiment = adjust_experiment_data(experiment)

variants = ['L', 'P', 'LP']


results = pd.DataFrame()

for variant in variants:
    # select correct variants
    matching_variants = experiment[(experiment['Variante'] == variant) & (experiment['q_Variante'] == variant)]
    mismatching_variants = experiment[(experiment['Variante'] == variant) & (experiment['q_Variante'] != variant)]

    matching_variants, mismatching_variants = matching_variants[items], mismatching_variants[items]

    # mean
    mistmach_name = variant + '_'
    results[variant] = matching_variants.mean()
    results[mistmach_name] = mismatching_variants.mean()
    # perform t-test
    for item in items:
        t_statistic = stats.ttest_ind(matching_variants[item], mismatching_variants[item], equal_var=False)
        results.loc[item, variant + ' t_value'] = t_statistic[0]
        results.loc[item, variant + ' p_value'] = t_statistic[1]


#count how many p values per item are lower than 0.05
list_p_values = ['L p_value', 'P p_value', 'LP p_value']
for p_value in list_p_values:
    results.loc['count_sig_p_values', p_value] = results[p_value][(results[p_value] <= 0.05)].count()

results.round(decimals=4).to_excel(os.path.join(data_handler.path_thesis, 'confusion t-test results.xlsx'), index=True)


# test
# len(matching_variants)
# Out[20]: 17
# len(matching_variants)
# Out[20]: 17
# len(experiment[(experiment['Variante'] == 'L')])
# Out[22]: 37
