import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from io import StringIO
import sys

import scipy.stats as stats
import researchpy as rp
import statsmodels.api as sm
from statsmodels.formula.api import ols

from src.DataHandler import FeatureDataHandler
from pipeline._8experiment.helper_evaluation import *

data_handler = FeatureDataHandler()

experiment = data_handler.load_experiment_data()
experiment = adjust_experiment_data(experiment)


constructs = constructs_items.keys()

# Perform Anova Test on the constructs
result_str = ''
result_df = pd.DataFrame(
    columns=['Konstrukt', 'Adj. R-squared', 'F-Stat.', 'p(F-Stat)', 'Intercept(I)', 'C(Variante)[O]', 'p(C(V)[O]',
             'C(Variante)[P]',
             'p(C(V)[P]', 'C(Variante)[LP]', 'p(C(V)[LP]', 'Jarque-Bera(JB)', 'p(JB)', 'Levene Test(L)', 'p(L)'])
result_summaries = {}

for variable_to_test in constructs:
    print('Testing for ' + variable_to_test)
    realtionship_to_test = "{konstrukt} ~C(Variante)".format(konstrukt=variable_to_test)

    # OLS
    results = ols(realtionship_to_test, data=experiment).fit()
    summary = results.summary()
    result_str += 'Geteste Variable: ' + realtionship_to_test + '\n'
    result_str += summary.as_text() + '\n'

    # Additional Table for Variance
    aov_table = sm.stats.anova_lm(results, typ=2)
    aov_table = anova_table(aov_table)
    result_str += 'Analysis of Variance Table' + '\n'
    result_str += aov_table.to_string() + '\n'

    # perform additional test, because they can't be extracted from the model (and summary

    # Stats Levene Test for homogenity of variance
    levene_test = stats.levene(experiment[variable_to_test][experiment['Variante'] == 'L'],
                               experiment[variable_to_test][experiment['Variante'] == 'O'],
                               experiment[variable_to_test][experiment['Variante'] == 'P'],
                               experiment[variable_to_test][experiment['Variante'] == 'LP'],
                               )
    result_str += str(levene_test) + '\n'

    # result_str += '==============================================================================' + '\n'
    # result_str += '==============================================================================' + '\n'
    result_str += '\n \n '

    # highly dependent on current version of statsmodel

    result_dict = {'Konstrukt': variable_to_test, 'Adj. R-squared': results.rsquared_adj, 'F-Stat.': results.fvalue,
                   'p(F-Stat)': results.f_pvalue, 'Intercept(I)': results.params[0], 'p(I)': results.pvalues[0],
                   'C(Variante)[O]': results.params[1], 'p(C(V)[O]': results.pvalues[1],
                   'C(Variante)[P]': results.params[2],
                   'p(C(V)[P]': results.pvalues[2], 'C(Variante)[LP]': results.params[3],
                   'p(C(V)[LP]': results.pvalues[3],
                   'Jarque-Bera(JB)': summary.tables[2].data[1][3], 'p(JB)': summary.tables[2].data[2][3],
                   'Levene Test(L)': levene_test.statistic, 'p(L)': levene_test.pvalue}
    result_df = result_df.append(result_dict, ignore_index=True)
    result_summaries[variable_to_test] = summary

    # result_str = result_str.replace('====================================================================================',
    #                    '===============================================')
    #
    # result_str = result_str.replace('==============================================================================',
    #                    '================================================')

result_df.iloc[:, 1:] = result_df.iloc[:, 1:].astype(float)

# Pretests
not_normaly_distributed = result_df[(result_df['p(JB)'] <= 0.05) | (result_df['p(JB)'] >= 0.95)]
Varianzhomogenität = result_df[(result_df['p(L)'] <= 0.05) | (result_df['p(L)'] >= 0.95)]

# write results
data_handler.write_evaluation_results_text('ANOVA_results_Anhang_Basis_L.txt', result_str)
# result_df['Konstrukt'] = result_df['Konstrukt'].apply(
#     lambda x: constructs_abbrevations[x])  # rename constructs to abbrevations
result_df.round(decimals=3).to_excel(os.path.join(data_handler.path_thesis, 'ANOVA Test results_Basis_L.xlsx'), index=False)
