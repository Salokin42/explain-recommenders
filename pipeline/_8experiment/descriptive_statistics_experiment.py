import os
import math
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from src.DataHandler import FeatureDataHandler
from src.FeatureHandler import FeatureHandler
from pipeline._8experiment.helper_evaluation import *
from sklearn import preprocessing

data_handler = FeatureDataHandler()
feature_handler = FeatureHandler()

experiment = data_handler.load_experiment_data()
recommendations = data_handler.load_experiment_recommendations()
ratings = data_handler.load_experiment_ratings()
explanations = data_handler.load_experiment_explanations()
top_user_features = data_handler.load_experiment_top_user_features()

### Experiment

## Descriptive statistics
variante_true = experiment['Variante']
variante_question = experiment['q_Variante']

variante_true.value_counts()
variante_question.value_counts()
# #Variante
# plt.figure(1,figsize=(8,2))
#
# plt.subplot(141)
# variante_true.value_counts().plot('bar')
#
# plt.subplot(142)
# plt.hist(variante_question.values)
#
# plt.show()


# Korrekt
variante_korrekt_angegeben = experiment[experiment['Variante'] == experiment['q_Variante']].shape
print('Variante korrekt angeeben haben {}'.format(variante_korrekt_angegeben[0]))

# confusion matrix
le = preprocessing.LabelEncoder()
le.fit(experiment['Variante'].values)

class_names = le.classes_

plot_confusion_matrix(le.transform(experiment['Variante']), le.transform(experiment['q_Variante']), classes=class_names,
                      title=' ')

plt.savefig(os.path.join(data_handler.path_thesis / 'Konfusionsmatrix Variante.jpg'))
plt.show()

# Plot distribution of confusions
variants = ['L', 'P', 'LP']
for variant in variants:
    matching_variants = experiment[(experiment['Variante'] == variant) & (experiment['q_Variante'] == variant)]
    mismatching_variants = experiment[(experiment['Variante'] == variant) & (experiment['q_Variante'] != variant)]

x = matching_variants['ErkL_1'].values
y = mismatching_variants['ErkL_1'].values

# Average answers per item per variant
average_staistics = experiment.groupby('Variante').mean()
overall_mean = experiment.mean()
overall_mean.rename('ALL', inplace=True)
average_staistics = average_staistics.append(overall_mean)
average_staistics = average_staistics.reindex(['ALL',  'O', 'L', 'P', 'LP'])
average_staistics = average_staistics[items + list(constructs_items.keys())]

average_staistics.round(decimals=3).to_excel(os.path.join(data_handler.path_thesis, 'average_statistics.xlsx'))

# average_staistics = average_staistics.transpose()
# draw

# general settings
max_rating, min_rating = average_staistics.max().max(), average_staistics.min().min()
upper_bound, lower_bound = 6,2#int(math.ceil(max_rating)),  int(round(min_rating))

color_variants = ['slategrey',  'black', 'green', 'lightskyblue', 'darkblue']

fig_per_row = 3

for z in range(0, len(items), fig_per_row):
    list_for_image = items[z:z + fig_per_row]
    f, axarr = plt.subplots(1, len(list_for_image), sharey=True)
    for i, item in enumerate(list_for_image):
        y = average_staistics[item]
        variant_names = y.index.tolist()
        x_pos = np.arange(len(variant_names))

        axarr[i].bar(x_pos, y, color=color_variants)

        axarr[i].set_title(item)
        axarr[i].set_xticks(x_pos)
        axarr[i].set_xticklabels(variant_names)
        axarr[i].set_xticks(x_pos)

        axarr[i].set_ylim(lower_bound, upper_bound)

        for c, v in enumerate(y):
            axarr[i].text(c - 0.5, v + 0.1, str('%.2f' % v), color='black')

    plt.savefig(os.path.join(data_handler.path_thesis / 'descritpive_statistics_items' / 'items_{0}.jpg'.format(z)),
                bbox_inches='tight')
    plt.show()
    plt.close('all')

#### for average constructs
fig_per_row = 3

for z in range(0, len(constructs_items.keys()), fig_per_row):
    list_for_image = list(constructs_items.keys())[z:z + fig_per_row]
    f, axarr = plt.subplots(1, len(list_for_image), sharey=True)
    for i, item in enumerate(list_for_image):
        y = average_staistics[item]
        variant_names = y.index.tolist()
        x_pos = np.arange(len(variant_names))

        axarr[i].bar(x_pos, y, color=color_variants)

        axarr[i].set_title(constructs_abbrevations[item])
        axarr[i].set_xticks(x_pos)
        axarr[i].set_xticklabels(variant_names)
        axarr[i].set_xticks(x_pos)

        axarr[i].set_ylim(0, upper_bound)

        for c, v in enumerate(y):
            axarr[i].text(c - 0.5, v + 0.1, str('%.2f' % v), color='black')

    plt.savefig(os.path.join(data_handler.path_thesis / 'descritpive_statistics_items' / 'constructs{0}.jpg'.format(z)),
                bbox_inches='tight')
    plt.show()
    plt.close('all')

### control_variables
control = experiment[['q_Alter', 'q_Bildung', 'q_Geschlecht',
                           'q_Vorerfahrung']]
control = control.astype('category')
control_statistics = control.apply(pd.value_counts)

control_statistics.round(decimals=3).to_excel(os.path.join(data_handler.path_thesis, 'control_statistics.xlsx'))


#for boxplots of average constructs

#single boxplot
experiment = sort_O_first(experiment)
plt.close('all')
constructs = list(constructs_items.keys())
plt.figure()
experiment.boxplot(column=constructs, by='Variante', figsize=(20,30), grid=True, layout=(4,3))
plt.margins(int(10000.00))

plt.savefig(os.path.join(data_handler.path_thesis / 'descritpive_statistics_items' / 'construct_boxplots.jpg'.format(z)),
                bbox_inches='tight')
plt.show()
plt.close('all')







# debug

###____ diff

# width = 0.8
# for item in items:
#     plt.title(item)
#     y = average_staistics[item]
#     variant_names = y.index.tolist()
#     x_pos = np.arange(len(variant_names))
#
#     plt.bar(x_pos, y, width=0.8, color=['blue', 'green', 'black', 'purple', 'red'])
#     plt.xticks(x_pos, variant_names)
#
#     plt.ylim(0, upper_bound)
#
#     ax = plt.axes()
#     for i, v in enumerate(y):
#         ax.text(i - 0.15, v + 0.15, str('%.2f' % v), color='black')
#     plt.gca().axes.get_yaxis().set_visible(False)
#
#     plt.savefig(os.path.join(data_handler.path_thesis / 'descritpive_statistics_items' / '{0}.jpg'.format(item)))
#     plt.show()
#     plt.close()

# ____


#
# fig, ax = plt.subplots()
# rects1 = ax.bar(ind - width/2, x, width, yerr=x,
#                 color='SkyBlue', label='Men')
#
#
# # Add some text for labels, title and custom x-axis tick labels, etc.
# ax.set_ylabel('Scores')
# ax.set_title('Scores by group and gender')
# ax.set_xticks(ind)
# ax.set_xticklabels(('G1', 'G2', 'G3', 'G4', 'G5'))
# ax.legend()
#
#
# def autolabel(rects, xpos='center'):
#     """
#     Attach a text label above each bar in *rects*, displaying its height.
#
#     *xpos* indicates which side to place the text w.r.t. the center of
#     the bar. It can be one of the following {'center', 'right', 'left'}.
#     """
#
#     xpos = xpos.lower()  # normalize the case of the parameter
#     ha = {'center': 'center', 'right': 'left', 'left': 'right'}
#     offset = {'center': 0.5, 'right': 0.57, 'left': 0.43}  # x_txt = x + w*off
#
#     for rect in rects:
#         height = rect.get_height()
#         ax.text(rect.get_x() + rect.get_width()*offset[xpos], 1.01*height,
#                 '{}'.format(height), ha=ha[xpos], va='bottom')
#
#
# autolabel(rects1, "left")
#
# plt.show()
# plt.close()
