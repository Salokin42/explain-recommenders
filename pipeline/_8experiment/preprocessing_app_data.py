import os
import numpy as np
import pandas as pd

from src.DataHandler import FeatureDataHandler
from src.FeatureHandler import FeatureHandler

data_handler = FeatureDataHandler()
feature_handler = FeatureHandler()

recommendations = data_handler.load_experiment_recommendations()
ratings = data_handler.load_experiment_ratings()
explanations = data_handler.load_experiment_explanations()
top_user_features = data_handler.load_experiment_top_user_features()

#replace feature names in top_user_features
top_user_feature_names = feature_handler.replace_feature_descriptions(top_user_features['explanation_term'].tolist(),data_handler)
top_user_features['explanation_term'] = top_user_feature_names
top_user_features.to_csv(os.path.join(data_handler.path_evaluation,'top_user_features.csv'), index=False)