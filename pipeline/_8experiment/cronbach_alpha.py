import pandas as pd

from src.DataHandler import FeatureDataHandler
from pipeline._8experiment.helper_evaluation import *

data_handler = FeatureDataHandler()

experiment = data_handler.load_experiment_data()
experiment = adjust_experiment_data(experiment)

constructs = constructs_items.keys()


def cronbach_alpha(items):
    items = pd.DataFrame(items)
    items_count = items.shape[1]
    variance_sum = float(items.var(axis=0, ddof=1).sum())
    total_var = float(items.sum(axis=1).var(ddof=1))

    return (items_count / float(items_count - 1) *
            (1 - variance_sum / total_var))


results = {}
for construct, items in constructs_items.items():
    if len(items) <= 1:
        continue
    results[construct] = cronbach_alpha(experiment[items])

conbach_alphas_df = pd.Series(results)
