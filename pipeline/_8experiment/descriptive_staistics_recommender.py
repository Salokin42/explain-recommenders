import os
import math
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from src.DataHandler import FeatureDataHandler
from src.FeatureHandler import FeatureHandler
from pipeline._8experiment.helper_evaluation import *
from sklearn import preprocessing

data_handler = FeatureDataHandler()
feature_handler = FeatureHandler()

experiment = data_handler.load_experiment_data()
recommendations = data_handler.load_experiment_recommendations()
ratings = data_handler.load_experiment_ratings()
explanations = data_handler.load_experiment_explanations()
top_user_features = data_handler.load_experiment_top_user_features()

### nur 147 > Am Anfang des Kapitels_ Aufgrund eines technischen Fehlers wurden die Daten
# des Empfehlungsdienstes für 3/150 Teilnehmern nicht abgespeichert. Da diese nur deskriptiv beschrieben werden
# aber nicht in die weitere Analyse einfließen, stellt dies kein Problem dar.


# select only those used in experiment
recommendations = recommendations.merge(experiment, left_on=['userId'], right_index=True).loc[:, :'Variante']
ratings = ratings[ratings['userId'].isin(recommendations['userId'])]

# top 20 stats
top_recommendations = recommendations['movieId'].value_counts() / (len(recommendations) / 3)
top_recommendations.index = data_handler.add_movie_information(top_recommendations.index)['title']
top_recommendations.round(decimals=4).to_excel(os.path.join(data_handler.path_thesis, 'top_recommendations.xlsx'))

# top_lime_explanations
top_lime_userIds = recommendations[(recommendations['Variante'] == 'L') | (recommendations['Variante'] == 'LP')][
    'userId'].unique()
top_lime_explanations = explanations[explanations['userId'].isin(top_lime_userIds)]
top_lime_explanations = top_lime_explanations['explanation_term'].value_counts()
top2 = top_lime_explanations / len(top_lime_userIds)
top2.round(decimals=3).to_excel(os.path.join(data_handler.path_thesis, 'top_lime_explanations.xlsx'))
# Ein Nutzer der Variante L oder LP hat das Tag ca so oft gesehen


# top_preferences

top_preferences_userIds = recommendations[(recommendations['Variante'] == 'P') | (recommendations['Variante'] == 'LP')][
    'userId'].unique()
top_preferences = top_user_features[top_user_features['userId'].isin(top_preferences_userIds)]
top_preferences = top_preferences['explanation_term'].value_counts()
top3 = top_preferences / (len(top_preferences_userIds))
top3.round(decimals=3).to_excel(os.path.join(data_handler.path_thesis, 'top_preferences.xlsx'))

# rating

plt.clf()
# Histogram for ratings
bins = np.arange(0.5, 6, 0.5)

plt.hist(ratings['rating'], align='left', bins=bins)

plt.ylabel("Anzahl")
plt.xlabel('Sterne Bewertung')

plt.xticks(bins[:-1])
plt.grid(b=True)

plt.savefig(os.path.join(data_handler.path_thesis / 'ratings_experiment.jpg'),
            bbox_inches='tight')
plt.show()
plt.clf()
