import os
import numpy as np
import pandas as pd

from src.DataHandler import FeatureDataHandler

data_handler = FeatureDataHandler()

# load data
survey = data_handler.load_experiment_data_raw(filename='data_project_760168_2019_04_25.xls')

# labels
survey.set_index('lfdn', inplace=True)
survey.rename({'quota_assignment': 'Variante', 'v_584': 'q_Variante', 'v_135': 'q_Geschlecht', 'v_136': 'q_Alter',
               'v_583': 'q_Bildung', 'v_585': 'q_Vorerfahrung'}, axis='columns', inplace=True)

# replace values
survey['Variante'].replace({1: 'O', 2: 'L', 3: 'P', 4: 'LP'}, inplace=True)
survey['q_Variante'].replace({6: 'O', 7: 'L', 8: 'P', 9: 'LP'}, inplace=True)

survey['q_Geschlecht'].replace({1: 'Männlich', 2: 'Weiblich', 3: 'k.A.'}, inplace=True)
survey['q_Alter'].replace({1: '18-24', 2: '25-34', 3: '35-44', 4: '45-59', 5: '60+'}, inplace=True)
survey['q_Bildung'].replace({6: '(Noch) kein Schulabschluss', 7: 'Hauptschulabschluss oder vergleichbar',
                             8: 'Realschulabschluss, Mittlere Reife oder vergleichbar',
                             9: 'Allgemeine Hochschulreife, Fachhochschulreife oder vergleichbar',
                             10: 'Abgeschlossenes Bachelorstudium', 11: 'Abgeschlossenes Master-/ Diplomstudium',
                             12: 'Abgeschlossene Promotion', 13: 'k.A.'}, inplace=True)

survey['q_Vorerfahrung'].replace(
    {1: 'Keine Erfahrung', 2: 'Wenig Erfahrung', 3: 'Durschnittliche Erfahrung', 4: 'Viel Erfahrung',
     5: 'Sehr viel Erfahrung'}, inplace=True)

# Bring same questions in same column
Q_V0 = survey.loc[(survey['Variante'] == 'O'), 'v_648':'v_675']
Q_VL = survey.loc[(survey['Variante'] == 'L'), 'v_620':'v_647']
Q_VP = survey.loc[(survey['Variante'] == 'P'), 'v_592':'v_619']
Q_VLP = survey.loc[(survey['Variante'] == 'LP'), 'v_559':'v_586']

questions_list = [Q_V0, Q_VL, Q_VP, Q_VLP]
column_names = ['Q{n}'.format(n=n) for n in range(1, 29)]
for q in questions_list:
    q.columns = column_names
questions = pd.concat(questions_list)

survey = survey.join(questions)

# rename the questions
survey.rename({
    "Q1": "ErkL_1",
    "Q2": "ErkP_1",
    "Q3": "ErkP_2",
    "Q4": "Tra_1",
    "Q5": "Tra_2",
    "Q6": "Tra_3",
    "Q7": "GenauE_1 ",
    "Q8": "GenauE_2",
    "Q9": "GenauE_3",
    "Q10": "NeuE_1",
    "Q11": "VerschE_1",
    "Q12": "Nütz_1",
    "Q13": "Nütz_2",
    "Q14": "Nütz_3",
    "Q15": "EmpT_1",
    "Q16": "Dummy",
    "Q17": "EmpT_2",
    "Q18": "CompT_1",
    "Q19": "CompT_2",
    "Q20": "CompT_3",
    "Q21": "CompT_4",
    "Q22": "BeneT_1",
    "Q23": "BeneT_2",
    "Q24": "BeneT_3",
    "Q25": "InteT_1",
    "Q26": "InteT_2",
    "Q27": "InteT_3",
    "Q28": "Zusatz_Intention"
}, axis='columns', inplace=True)

## subselect again in specific order

# Select useful columns
# survey_subset = survey[
#     list(survey.loc[:, 'Q1':'Q28'] + survey.loc[:, 'q_Variante':'q_Vorerfahrung']) + ['Variante', 'duration',
#                                                                                       'datetime']]
# order = survey_subset.loc[:, 'q_Variante':'datetime'].columns.tolist()

order = ['Variante',
         "ErkL_1",
         "ErkP_1",
         "ErkP_2",
         "Tra_1",
         "Tra_2",
         "Tra_3",
         "GenauE_1 ",
         "GenauE_2",
         "GenauE_3",
         "NeuE_1",
         "VerschE_1",
         "Nütz_1",
         "Nütz_2",
         "Nütz_3",
         "EmpT_1",
         "Dummy",
         "EmpT_2",
         "CompT_1",
         "CompT_2",
         "CompT_3",
         "CompT_4",
         "BeneT_1",
         "BeneT_2",
         "BeneT_3",
         "InteT_1",
         "InteT_2",
         "InteT_3",
         "Zusatz_Intention",
         'q_Alter',
         'q_Bildung',
         'q_Geschlecht',
         'q_Variante',
         'q_Vorerfahrung',
         'duration',
         'datetime']
survey_subset = survey[order]

data_handler.save_experiment_data(survey_subset, 'experiment_complete.csv')

# Additional
# _______________

# Add empty columns
# column_names = [*survey.columns.tolist()] + ['Q{n}'.format(n=n) for n in range(1, 29)]
# survey = survey.reindex(columns=column_names)
# #
# # survey.loc[(survey['Variante' == 'O']):, 'Q1':'Q28'] = survey.loc[(survey['Variante' == 'O']):, 'v_648':'v_675']
# #
# survey.loc[(survey['Variante'] == 'O'), 'Q1':'Q28'] = survey.loc[(survey['Variante'] == 'O'), 'v_648':'v_675']
# #
# def select_questions(survey_data_row):
#     variant = survey_data_row['Variante']
#     if variant == 'O':
#         questions = survey_data_row['v_648':'v_675']
#     elif variant == 'L':
#         questions = survey_data_row['v_620':'v_647']
#     elif variant == 'P':
#         questions = survey_data_row['v_592':'v_619']
#     elif variant == 'LP':
#         questions = survey_data_row['v_559':'v_586']
#
#     survey_data_row['Q1':'Q28'] = questions
#
#
# survey = survey.apply(lambda x: select_questions(x), axis=1)
