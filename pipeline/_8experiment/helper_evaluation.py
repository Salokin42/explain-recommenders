import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

from sklearn import svm, datasets
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
from sklearn.utils.multiclass import unique_labels

# dicts of construct > items
constructs_items = {
    'Erklärung_der_Empfehlung': ['ErkL_1'],
    'Erklärung_System': ['ErkP_1', 'ErkP_2'],
    'Genauigkeit_der_Empfehlung': ['GenauE_1 ', 'GenauE_2', 'GenauE_3'],
    'Neuartigkeit_der_Empfehlung': ['NeuE_1'],
    'Verschiedenartigkeit_der_Empfehlung': ['VerschE_1'],
    'Transparenz': ['Tra_1', 'Tra_2', 'Tra_3'],
    'Wahrgenommene_Nützlichkeit': ['Nütz_1', 'Nütz_2', 'Nütz_3'],
    'Trust_Empfehlung': ['EmpT_1', 'EmpT_2'],
    'Trust_Competence': ['CompT_1', 'CompT_2', 'CompT_3', 'CompT_4'],
    'Trust_Benevolence': ['BeneT_1', 'BeneT_2', 'BeneT_3'],
    'Trust_Integrity': ['InteT_1', 'InteT_2', 'InteT_3']
}
items = list(constructs_items.values())
items = [item for sublist in items for item in sublist]

constructs_abbrevations = {
    'Erklärung_der_Empfehlung': 'EL',
    'Erklärung_System': 'ES',
    'Genauigkeit_der_Empfehlung': 'E_G',
    'Neuartigkeit_der_Empfehlung': 'E_N',
    'Verschiedenartigkeit_der_Empfehlung': 'E_V',
    'Transparenz': 'T',
    'Wahrgenommene_Nützlichkeit': 'N',
    'Trust_Empfehlung': 'TE_',
    'Trust_Competence': 'TC',
    'Trust_Benevolence': 'TB',
    'Trust_Integrity': 'TI'
}


def adjust_experiment_data(experiment_df):
    experiment_df['Variante'] = pd.Categorical(experiment_df['Variante'], ['L', 'O', 'P', 'LP'])
    experiment_df.sort_values('Variante', inplace=True)

    return experiment_df


def sort_O_first(experiment_df):
    experiment_df['Variante'] = pd.Categorical(experiment_df['Variante'], ['O', 'L', 'P', 'LP'])
    experiment_df.sort_values('Variante', inplace=True)

    return experiment_df


def plot_confusion_matrix(y_true, y_pred, classes,
                          normalize=False,
                          title=None,
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if not title:
        if normalize:
            title = 'Normalized confusion matrix'
        else:
            title = 'Konfusionsmatrix (absolute Werte)'

    # Compute confusion matrix
    cm = confusion_matrix(y_true, y_pred)
    # Only use the labels that appear in the data
    classes = classes[unique_labels(y_true, y_pred)]
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    fig, ax = plt.subplots()
    im = ax.imshow(cm, interpolation='nearest', cmap=cmap)
    ax.figure.colorbar(im, ax=ax)
    # We want to show all ticks...
    ax.set(xticks=np.arange(cm.shape[1]),
           yticks=np.arange(cm.shape[0]),
           # ... and label them with the respective list entries
           xticklabels=classes, yticklabels=classes,
           title=title,
           ylabel='Variante Wahr',
           xlabel='Variante Fragebogen')

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")

    # Loop over data dimensions and create text annotations.
    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i in range(cm.shape[0]):
        for j in range(cm.shape[1]):
            ax.text(j, i, format(cm[i, j], fmt),
                    ha="center", va="center",
                    color="white" if cm[i, j] > thresh else "black")
    fig.tight_layout()
    return ax


def anova_table(aov):
    aov['mean_sq'] = aov[:]['sum_sq'] / aov[:]['df']

    aov['eta_sq'] = aov[:-1]['sum_sq'] / sum(aov['sum_sq'])

    aov['omega_sq'] = (aov[:-1]['sum_sq'] - (aov[:-1]['df'] * aov['mean_sq'][-1])) / (
            sum(aov['sum_sq']) + aov['mean_sq'][-1])

    cols = ['sum_sq', 'df', 'mean_sq', 'F', 'PR(>F)', 'eta_sq', 'omega_sq']
    aov = aov[cols]
    return aov


def cm2inch(*tupl):
    inch = 2.54
    if isinstance(tupl[0], tuple):
        return tuple(i/inch for i in tupl[0])
    else:
        return tuple(i/inch for i in tupl)
