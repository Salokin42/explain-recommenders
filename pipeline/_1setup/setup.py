from DataHandler import *

datasets = ['ml-latest-small', 'ml-20m']
#datasets = ['ml-latest-small']

for dataset in datasets:
    dh = DataHandler(dataset)
    dh.setup_data_folders()
    # dh.fetch_ml_data()
    dh.setup_data()
