import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from IPython.display import display, HTML

from DataHandlerFeatures import DataHandlerFeatures

data_handler = DataHandlerFeatures()
batch_generator = data_handler.build_generator(batch_size=5000)

pd.set_option('display.max_rows', -1)
# pd.reset_option('all')

# out of that junk 33 ratings have no genome scores

for X_chunk, y_cunk in batch_generator:
    # model.partial_fit(X_chunk,y_cunk)
    break

X_chunk.isna().sum()

movies_metadata = data_handler.load_movie_features()

# Genome Tags are missing from 16897 movies out of 27278 movies
print(movies_metadata.isna().sum())
print(movies_metadata.shape)

movies = data_handler.load_movies()
movies.columns

movies['year'] = movies['title'].str.extract('(\d\d\d\d)')

movie_ids_without_genome_tag = movies_metadata[movies_metadata.loc[:,'1128'].isnull()].loc[:,'movieId']
movies_without_genome = movies[movies['movieId'].isin(movie_ids_without_genome_tag)]
movies_plot = movies_without_genome['year'].value_counts().sort_index()



#check if user columns are ever nan
user_data = X_chunk.filter(regex=("user.*"))
user_data.isna().sum().sum()