import os
import logging
import pandas as pd
import numpy as np

from model_selection_helpers import *
from src.DataHandler import DataHandler
from src.FeatureHandler import FeatureHandler
from src.Models.SGDBatchRegressor import SGDBatchRegressor
from sklearn.model_selection import GroupKFold, cross_val_score, KFold
from sklearn.model_selection import GridSearchCV, RandomizedSearchCV

from joblib import dump

logger = logging.getLogger()
logger.setLevel(level=logging.DEBUG)

data_handler = DataHandler()
feature_handler = FeatureHandler()

ratings = data_handler.load_ratings_dev()
# ratings = data_handler.load_ratings_dev()[0:20000]  # use for faster test
user_features = data_handler.load_user_features_subset_dev()
movie_features = data_handler.load_movie_features_subset()

ratings = FeatureHandler.drop_ratings_where_movie_no_genome_tag(ratings, data_handler.load_movie_features())
ratings = ratings.sample(frac=1)  # shuffle
X = ratings.loc[:, ['userId', 'movieId']]
y = ratings['rating']

batch_sgd = SGDBatchRegressor()

gkf = GroupKFold(n_splits=2)
param_grid = {
    'alpha': 10.0 ** -np.arange(1, 5),
    'eta0': 10.0 ** -np.arange(1, 5),
    'power_t': [0.10, 0.15, 0.2],
    'loss': ['huber'],
    'penalty': ['l1'],
    'learning_rate': ['constant', 'invscaling']
}
groups = ratings['userId']
scoring = 'neg_mean_absolute_error'  # negated - the higher the better


def cross_validation(model, scoring):
    scores = cross_val_score(model, X=X, y=y, scoring=scoring, cv=gkf, groups=groups)
    return scores


def param_search(param_grid, n_iter=10):
    clf = RandomizedSearchCV(batch_sgd, n_iter=n_iter, param_distributions=param_grid, cv=gkf, scoring=scoring,
                             verbose=2)
    clf.fit(X, y, movie_features=movie_features, user_features=user_features, groups=groups)
    store_param_search_results([clf], data_handler)
    for param_name in param_grid.keys():
        GridSearch_table_plot(clf, param_name)
    return clf


# Param search
# run 1
# param_grid = {
#     'alpha': 10.0 ** -np.arange(1, 7),
#     'loss': ['squared_loss', 'huber', 'epsilon_insensitive'],
#     'eta0': 10.0 ** -np.arange(1, 5),
#     'power_t': [0.10, 0.15, 0.2],
#     'penalty': ['l2', 'l1', 'elasticnet'],
#     'learning_rate': ['constant', 'optimal', 'invscaling'],
# }
#
# clf = param_search(param_grid, n_iter=20)
# results = pd.DataFrame(clf.cv_results_).sort_values(by="rank_test_score")

best_params = {'power_t': 0.1,
               'penalty': 'elasticnet',
               'loss': 'squared_loss',
               'learning_rate': 'constant',
               'eta0': 0.01,
               'alpha': 0.01}

clf = SGDBatchRegressor(**best_params)
scores = cross_val_score(clf, X=X, y=y, cv=5, scoring='neg_mean_squared_error',
                         fit_params={'user_features': user_features,
                                     'movie_features': movie_features})

clf.fit(X, y, movie_features, user_features)
#dump(clf, os.path.join(data_handler.path_model, 'SGDBatchRegressor_subset_temp.joblib'))

