import os
import logging
import pandas as pd

from src.DataHandler import DataHandler
from src.FeatureHandler import FeatureHandler
from src.Models.SGDBatchRegressor import SGDBatchRegressor
from sklearn.model_selection import GroupKFold, cross_val_score, KFold
from sklearn.model_selection import GridSearchCV, RandomizedSearchCV

logger = logging.getLogger()
logger.setLevel(level=logging.DEBUG)

data_handler = DataHandler()
feature_handler = FeatureHandler()

ratings = data_handler.load_ratings_dev()
# ratings = data_handler.load_ratings_dev()[0:500]  # use for faster test
user_features = data_handler.load_user_features_dev_raw()
movie_features = data_handler.load_movie_features_raw()

ratings = FeatureHandler.drop_ratings_where_movie_no_genome_tag(ratings, data_handler.load_movie_features())
ratings = ratings.sample(frac=1)  # shuffle
X = ratings.loc[:, ['userId', 'movieId']]
y = ratings['rating']

user_features = user_features.dropna()
movie_features = movie_features.dropna()
avg_user_rating = user_features['user_avg_rating']
user_features.drop(labels=['user_avg_rating'], axis=1, inplace=True)
# user_features = pd.DataFrame(MinMaxScaler(feature_range=(0, 1)).fit_transform(user_features), index=user_features.index,
#                              columns=user_features.columns)  # Scale to [0,1]

# clf = SGDBatchRegressor(batch_size=10000)
#
# logging.info('Starting Fitting')
#
# # clf.fit(X=X, y=y,  movie_features=movie_features, user_features=user_features)
#
# # dump(clf, os.path.join(data_handler.path_model, 'SGDBatchRegressor_dev_temp.joblib'))
#
# # # KFold Cross-Val> With precalculated User Features , random 3-Fold split, on ratings_dev
# # # scores.mean()
# # # Out[4]: -0.8264313458550246
#
#
# # build sub_feature_sets
# sort_out = {'464': 'great', '445': 'good', '1072': 'very good', '364': 'excellent' }
# movie_features.drop(sort_out.keys(), axis=1, inplace=True)
# ü = movie_features.loc[:, "1":].mean().sort_values(ascending=False)
# feature_subset_columns = {}
# # for i in [100, 200, 300, 500, len(movie_features_no_genres_mean)]:
# for i in [100, 200, 300]:
#     cur_features = movie_features.loc[:, :'Western'].columns.tolist()  # always use genres
#     top_i_genome_features = movie_features_no_genres_mean.index[:i].tolist()
#     feature_subset_columns[i] = cur_features + top_i_genome_features
#
#     # cur_features = pd.concat([cur_features, movie_features[top_i_genome_features]],
#     #                          axis=1)  # add the top i genome_tag_features
#
# # iterate through all sub_feature_sets
# # evaluation_results = {}
# # for n, subset in feature_subset_columns.items():
# #     logging.info("Evaluating with {n} genome features".format(n=n))
# #     user_subset = ["user_" + x for x in subset]
# #     scores = cross_val_score(clf, X=X, y=y, cv=2, scoring='neg_mean_squared_error',
# #                              fit_params={'user_features': user_features[user_subset],
# #                                          'movie_features': movie_features[subset]})
# #     evaluation_results[n] = scores.mean()
#
# # save results
# # df = pd.DataFrame(evaluation_results, index=[0])
# # df.to_csv(os.path.join(data_handler.path_processed, 'evaluation_subset_features.csv'))
#
# # >> Use 200 genome features, more results in the csv file
# # sub_select top 200
# # sub = movie_features[subset]
# # sub.columns = FeatureHandler.replace_feature_descriptions(sub.columns, data_handler)
# # sort_out = {464: 'great', 445: 'good', 1072: 'very good', 364: 'excellent'}
#
# # Fit with filtered 100 > Check if weights matter
# subset = feature_subset_columns[100]
# user_subset = ["user_" + x for x in subset]
# clf.fit(X=X, y=y, user_features=user_features[user_subset],
#         movie_features=movie_features[subset])
#
#
# path = os.path.join(data_handler.path_processed, 'user_features_subset_dev.csv')
# user_features[user_subset].to_csv(path, header=True)
#
# path = os.path.join(data_handler.path_processed, 'movie_features_subset.csv')
# movie_features[subset].to_csv(path, header=True)
#
#
# # add names to coef
# index = FeatureHandler.replace_feature_descriptions(clf.feature_handler.movie_features.columns, data_handler)
# coef = pd.Series(clf.coef_, index=index)
# coef.sort_values(ascending=False)