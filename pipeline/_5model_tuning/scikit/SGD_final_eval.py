import os
import logging
import pandas as pd
import numpy as np

from model_selection_helpers import *
from src.DataHandler import DataHandler
from src.FeatureHandler import FeatureHandler
from src.Models.SGDBatchRegressor import SGDBatchRegressor
from sklearn.metrics import mean_squared_error

from joblib import dump

logger = logging.getLogger()
logger.setLevel(level=logging.DEBUG)

data_handler = DataHandler()
feature_handler = FeatureHandler()

ratings = data_handler.load_ratings_test()
# ratings = data_handler.load_ratings_dev()[0:20000]  # use for faster test
#user_features = data_handler.load_user_features_subset_dev()
movie_features = data_handler.load_movie_features_subset()

ratings = FeatureHandler.drop_ratings_where_movie_no_genome_tag(ratings, data_handler.load_movie_features())

# Construct Test Set with 200 unseen users
unique_userIds = ratings['userId'].unique()
userIds_to_use = unique_userIds[800:1000]
ratings = ratings[ratings['userId'].isin(userIds_to_use)]
# df = ratings.groupby('userId')['userId'].count() #df to check how many ratings there are per user


ratings = ratings.sample(frac=1)  # shuffle
X = ratings.loc[:, ['userId', 'movieId']]
y = ratings['rating']

model = data_handler.load_model()
user_features_new = FeatureHandler.build_user_features(X, y, model.feature_handler.movie_features)
model.feature_handler.append_user_features(user_features_new)

# For Model
y_pred = model.predict(X=X)
score_model = mean_squared_error(y, y_pred)

#for mean
ratings_train = data_handler.load_ratings_dev()
rating_train = ratings_train['rating']
rating_train_mean = rating_train.mean()
score_mean = mean_squared_error(y, np.repeat(rating_train_mean,len(y)))

scores = pd.Series([score_model, score_mean, score_model-score_mean], index=["RMSE score model", "RMSE score mean", "Difference"])

from pathlib import Path
FIGURE_PATH= Path('/media/sf_PycharmProjects_Windows/Bilder_MT')

scores.to_excel(FIGURE_PATH / 'eval_on_testset.xlsx')


#dump(clf, os.path.join(data_handler.path_model, 'SGDBatchRegressor_subset_temp.joblib'))

