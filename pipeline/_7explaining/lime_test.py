import os
import logging

import pandas as pd
import lime
import lime.lime_tabular
from joblib import load



from src.DataHandler import DataHandler
from src.FeatureHandler import FeatureHandler
from src.Models.SGDBatchRegressor import SGDBatchRegressor, InteractionFeatureTransformer


logger = logging.getLogger()
logger.setLevel(level=logging.DEBUG)

data_handler = DataHandler()
logging.info('Loading Data')
movie_features = data_handler.load_movie_features_raw()
ratings = data_handler.load_ratings_dev()[0:500]
ratings = FeatureHandler.drop_ratings_where_movie_no_genome_tag(ratings, movie_features)
#ratings = ratings.sample(frac=1)  # shuffle
X = ratings.loc[:, ['userId', 'movieId']]
y = ratings['rating']

logging.info('Loading Model')
#clf = SGDBatchRegressor()  # Just for Code completion in pycharm
clf = load(os.path.join(data_handler.path_model, 'SGDBatchRegressor_dev.joblib'))
#interaction_feature_transformer = InteractionFeatureTransformer()# Just for Code completion in pycharm
interaction_feature_transformer = clf.interaction_feature_transformer

logging.info('Transforming Training Data')
X_feat = interaction_feature_transformer.transform(X)
logging.info("Shape of X_feat is: {0}".format(X_feat.shape))
X_feat_array = X_feat.values
feature_names = X_feat.columns

logging.info('Initializing Explainer')
#FIXME remove xfeat array
explainer = lime.lime_tabular.LimeTabularExplainer(X_feat_array, mode='regression', feature_names=feature_names,
                                                   random_state=42,
                                                   discretize_continuous=True)


ratings_sub = X_feat.iloc[0]

explanation = explainer.explain_instance(ratings_sub.values, clf.predict, num_features=20)
explanation.as_pyplot_figure().show()

user_features = clf.interaction_feature_transformer.user_features
movie_features = clf.interaction_feature_transformer.movie_features

