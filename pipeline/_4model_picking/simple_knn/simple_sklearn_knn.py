from DataHandler import *
from sklearn.neighbors import NearestNeighbors

dh = DataHandler('ml-latest-small')
trainset, testset, movies = dh.load_ratings_train(), dh.load_ratings_test(), dh.load_movies()

train_matrix, test_matrix = trainset.pivot(index='userId', columns='movieId', values='rating'), testset.pivot(
    index='userId', columns='movieId', values='rating')
train_matrix, test_matrix = train_matrix.fillna(value=0), test_matrix.fillna(value=0)

model_knn = NearestNeighbors(metric='cosine', algorithm='brute')
model_knn.fit(train_matrix)

## build a list that ranks the movies from neighbours

# for neighbours get movie_ids and ratings
# rate the movie with the

# test = test_matrix.iloc[1]
# test = test.loc[test != 0]
test_id = 1

#print("For user %s" % movies.loc[movies['movieId'] == test_id, 'title'])

distances, neighbours = model_knn.kneighbors([train_matrix.iloc[test_id]])

for neighbour in np.nditer(neighbours):
    train_matrix.loc[train_matrix['neighbour
    #select by index
    # take all movie ids & ratings
    # packe in tabelle und gewichte mit nähe

# Sortiere Tabelle



# for neighbour in np.nditer(neighbours):
#     print(movies.loc[movies['movieId'] == neighbour, 'title'])
#
# test_matrix

# test_matrix.where(test_matrix == 0).isna().sum().sum() > 20168
# train_matrix.where(train_matrix == 0).isna().sum().sum() >  80668
# trainset['movieId'].nunique()
