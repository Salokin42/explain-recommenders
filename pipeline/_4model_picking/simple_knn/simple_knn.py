#
# predict_new_movies
# input: user_ratings
# output: up to n recommended movies with score as explanation
#
# CF mit KNN-Model:
# Finds k-neigherst neighbors
# Predicts next movies by averaging the rating of those users and outputs the top n 3
#
# Erklärung der Form: k nächsten Nachbarn bewerten den Film durchschnittlich mit s Sternen
#
#
# Steps:
# 1. User-Movie, Rating Interaction Matrix
# 2. KNN unsupervised anwenden
# 3. Neh
#
# 3. Evaluieren
# 4. prediction

from DataHandler import *
from surprise_wrapper import *
from sklearn.neighbors import KNeighborsClassifier
from surprise.prediction_algorithms import KNNBasic
from surprise.model_selection import PredefinedKFold

dh = DataHandler('ml-latest-small')
trainset, testset = build_surprise_data(dh.load_ratings_train()), build_surprise_data(dh.load_ratings_test())

#train_matrix, test_matrix = dh.build_user_movie_matrix(train_data), dh.build_user_movie_matrix(test_data)
# trainset = Dataset.construct_trainset(trainset)
# testset = Dataset.construct_testset(testset)


model = KNNBasic(sim_options={'user_based': True})

folds_files = [(trainset, testset)]
data = trainset


#rmse, mae = accuracy.rmse(predictions), accuracy.mae(predictions)
kf = KFold(n_splits=2)
for trainset, testset in kf.split(data):
    model.fit(trainset)
    predictions = model.test(testset)
    rmse, mae = accuracy.rmse(predictions), accuracy.mae(predictions)


def recommend_items_given_rating(user_ratings, n=8):
    #Benutze die .test methode, mit den user ratings als testset
    user_ratings = convert_to_surprise_testset(user_ratings)
    #Notizen zu Test:
    # gibt mir die predictions für movies wieder, die ich
    model.test(user_ratings)
    #Welche movieId? Gibt es eine innere andere?






