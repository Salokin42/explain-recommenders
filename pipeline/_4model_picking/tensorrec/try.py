from tensorrec import TensorRec
from tensorrec.eval import fit_and_eval
from tensorrec.representation_graphs import (
    LinearRepresentationGraph, ReLURepresentationGraph, NormalizedLinearRepresentationGraph
)
from tensorrec.loss_graphs import WMRBLossGraph, BalancedWMRBLossGraph
from tensorrec.prediction_graphs import (
    DotProductPredictionGraph, CosineSimilarityPredictionGraph, EuclideanSimilarityPredictionGraph
)
from tensorrec.util import append_to_string_at_point
from lightfm import datasets
import numpy as np
import scipy.sparse as sp

import logging

logging.getLogger().setLevel(logging.INFO)


def get_movielens_100k(min_positive_score=4, negative_value=0):
    movielens_100k_dict = datasets.fetch_movielens(indicator_features=True, genre_features=True)

    def flip_ratings(ratings_matrix):
        ratings_matrix.data = np.array([1 if rating >= min_positive_score else negative_value
                                        for rating in ratings_matrix.data])
        return ratings_matrix

    test_interactions = flip_ratings(movielens_100k_dict['test'])
    train_interactions = flip_ratings(movielens_100k_dict['train'])

    # Create indicator features for all users
    num_users = train_interactions.shape[0]
    user_features = sp.identity(num_users)

    # Movie titles
    titles = movielens_100k_dict['item_labels']

    return train_interactions, test_interactions, user_features, movielens_100k_dict['item_features'], titles


print("Starting")
train_interactions, test_interactions, user_features, item_features, _ = get_movielens_100k(negative_value=0)

print('train', train_interactions, 'test', test_interactions, 'user_feat', user_features, 'item_feat', item_features)
