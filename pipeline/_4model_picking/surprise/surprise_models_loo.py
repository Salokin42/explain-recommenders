# from https://github.com/khanhnamle1994/movielens/blob/master/Content_Based_and_Collaborative_Filtering_Models.ipynb
# %matplotlib inline

from DataHandler import DataHandler
from ResultHandler import Result, ResultHandler
from surprise_wrapper import *

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from sklearn.feature_extraction.text import TfidfVectorizer

from surprise import Dataset, Reader
from surprise.prediction_algorithms import *
from surprise import Dataset
from surprise import accuracy
from surprise.model_selection import KFold
from surprise.model_selection import LeaveOneOut
from surprise.dataset import *
from surprise.prediction_algorithms import SVDpp


pd.set_option('display.float_format', lambda x: '%.3f' % x)


DATASET = "ml-20m"

dh = DataHandler(DATASET)
rh = ResultHandler(DATASET+"-try")
#for small: build_surprise_data(dh.load_ratings_train())
data = build_surprise_data(dh.load_ratings_try())

all_models_simple_and_user_param = init_all_models_and_std_params()

for model_name, model in all_models_simple_and_user_param.items():
    if model is SVDpp():
        n_splits = 3
    else:
        n_splits = 10
    result = evaluate_model(DATASET, data, model, validation_method=LeaveOneOut(n_splits=n_splits))
    rh.save_result(result)
    rh.save_model(result)

