# from https://github.com/khanhnamle1994/movielens/blob/master/Content_Based_and_Collaborative_Filtering_Models.ipynb
# %matplotlib inline

from DataHandler import DataHandler
from ResultHandler import Result, ResultHandler
from surprise_wrapper import *

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from sklearn.feature_extraction.text import TfidfVectorizer

from surprise import Dataset, Reader
from surprise.prediction_algorithms import *
from surprise import Dataset
from surprise import accuracy
from surprise.model_selection import KFold
from surprise.dataset import *

pd.set_option('display.float_format', lambda x: '%.3f' % x)

dh = DataHandler("ml-20m")
data = build_surprise_data(dh.load_ratings_try())
rh = ResultHandler("ml-20m")

all_models_simple = init_all_models()
del all_models_simple['SVDpp']

for model_name, model in all_models_simple.items():
    result = evaluate_model('ml-20m-try', data, model)
    rh.save_result(result)
    rh.save_model(result)

sim_options = {'user_based': False}
models = {'KNNBasic': KNNBasic(sim_options=sim_options),
          'KNNWithMeans': KNNWithMeans(sim_options=sim_options),
          'KNNWithZScore': KNNWithZScore(sim_options=sim_options),
          'KNNBaseline': KNNBaseline(sim_options=sim_options)}

for model_name, model in models.items():
    result = evaluate_model('ml-20m-try', data, model, parameters="user_based': False")
    rh.save_result(result)
    rh.save_model(result)

for model_name, model in {'SVDpp': SVDpp()}.items():
    result = evaluate_model('ml-20m-try', data, model, 3)
    rh.save_result(result)
    rh.save_model(result)


