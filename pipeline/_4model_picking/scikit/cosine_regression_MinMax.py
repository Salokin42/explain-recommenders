import os
import datetime
import numpy as np
import pandas as pd
import logging

from scipy.spatial.distance import cosine
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.preprocessing import MinMaxScaler

from src.DataHandlerFeatures import DataHandlerFeatures
from src.DataHandler import DataHandler

data_handler = DataHandler('ml-20m')

ratings = data_handler.load_ratings_dev()
user_features = data_handler.load_user_features_dev_raw().dropna()
movie_features = data_handler.load_movie_features_raw().dropna()

ratings = ratings[
    ratings['movieId'].isin(movie_features.index)]  # delete ratings with incomplete complete movie_features
avg_user_rating = user_features['user_avg_rating']
user_features.drop(labels=['user_avg_rating'], axis=1, inplace=True)
user_features = pd.DataFrame(MinMaxScaler().fit_transform(user_features), index=user_features.index,
                             columns=user_features.columns)

cos_sim_matrix = pd.DataFrame(cosine_similarity(user_features, movie_features), index=user_features.index,
                              columns=movie_features.index)
# TODO try different distance metrics

ratings['cos_sim'] = ratings.apply(lambda x: cos_sim_matrix.loc[x['userId'], x['movieId']], axis=1)
print(np.corrcoef(ratings['rating'], ratings['cos_sim']))





# ratings['cos_sim'] = cos_sim_matrix.loc[ratings['userId'], ratings['movieId']] > should be better but does not work
# series = np.empty(len(ratings))
# ratings_copy = ratings

# for idx, row in ratings_copy.iterrows():
#     print(idx)
#     cos_sim = cos_sim_matrix.loc[row['userId'], row['movieId']]
#     series[idx] = cos_sim
#     # vll im konvertieringsprozess > komplett als serise versuchen
#
# ratings = pd.concat([ratings, pd.Series(series)], axis=1)

# check if assignment is correct


# rating = LinReg (cos_sim für u_f kombination )

# Own implementation for test
# cos_sim_matrix = pd.DataFrame(index=user_features.index, columns=movie_features.index)
# def dot(A, B):
#     return (sum(a * b for a, b in zip(A, B)))
# def cosine_similarity_self(a, b):
#     return dot(a, b) / ((dot(a, a) ** .5) * (dot(b, b) ** .5))
# for idx_u_feat, u_feat in user_features.iterrows():
#     for idx_m_feat, m_feat in movie_features.iterrows():
#         cos_sim_matrix.loc[idx_u_feat, idx_m_feat] = cosine_similarity_self(u_feat, m_feat)
#     #FIXME Delete if full run
#     break


def test_cos_sim(cos_sim_matrix):
    def dot(A, B):
        return (sum(a * b for a, b in zip(A, B)))

    def cosine_similarity_self(a, b):
        return dot(a, b) / ((dot(a, a) ** .5) * (dot(b, b) ** .5))

    print(cos_sim_matrix.shape == (user_features.shape[0], movie_features.shape[0]))
    print(
        np.allclose(cos_sim_matrix.loc[761, 1], cosine_similarity_self(user_features.loc[761], movie_features.loc[1])))
    print(np.allclose(cos_sim_matrix.loc[20136, 90],
                      cosine_similarity_self(user_features.loc[20136], movie_features.loc[90])))

# test_cos_sim(cos_sim_matrix)
