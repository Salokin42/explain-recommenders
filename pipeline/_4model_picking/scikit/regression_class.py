import os
import datetime
import numpy as np
import pandas as pd
import logging

from scipy.sparse.data import _data_matrix
from sklearn.model_selection import GroupKFold, cross_val_score, KFold
from sklearn.model_selection import GridSearchCV, RandomizedSearchCV
from sklearn.preprocessing import MinMaxScaler
from joblib import dump, load

from src.DataHandler import DataHandler
from src.FeatureHandler import FeatureHandler
from src.Models.SGDBatchRegressor import SGDBatchRegressor
from src.model_selection_helpers import *

logger = logging.getLogger()
logger.setLevel(level=logging.DEBUG)

data_handler = DataHandler()
feature_handler = FeatureHandler()

#ratings = data_handler.load_ratings_dev()
ratings = data_handler.load_ratings_dev()[0:500] #use for faster test
user_features = data_handler.load_user_features_dev_raw()
movie_features = data_handler.load_movie_features_raw()

ratings = FeatureHandler.drop_ratings_where_movie_no_genome_tag(ratings, movie_features)
ratings = ratings.sample(frac=1)  # shuffle
X = ratings.loc[:, ['userId', 'movieId']]
y = ratings['rating']

user_features = user_features.dropna()
movie_features = movie_features.dropna()
avg_user_rating = user_features['user_avg_rating']
user_features.drop(labels=['user_avg_rating'], axis=1, inplace=True)
# user_features = pd.DataFrame(MinMaxScaler(feature_range=(0, 1)).fit_transform(user_features), index=user_features.index,
#                              columns=user_features.columns)  # Scale to [0,1]
#
clf = SGDBatchRegressor(batch_size=10000)
logging.info('Starting Fitting')

clf.fit(X=X, y=y,  movie_features=movie_features, user_features=user_features)
#dump(clf, os.path.join(data_handler.path_model, 'SGDBatchRegressor_dev_temp.joblib'))

# # KFold Cross-Val> With precalculated User Features , random 3-Fold split, on ratings_dev
# # scores.mean()
# # Out[4]: -0.8264313458550246
# scores = cross_val_score(clf, X=X, y=y, cv=3, scoring='neg_mean_squared_error',
#                          fit_params={'user_features': user_features, 'movie_features': movie_features})
# scores.mean()

# KFold Cross-Val > With new calculated User Features , random 3-Fold split, on ratings_dev
# scores.mean()
# Out[3]: -0.9759524885221839
# scores = cross_val_score(clf, X=X, y=y, cv=3, scoring='neg_mean_squared_error',
#                          fit_params={'user_features': None, 'movie_features': movie_features})
# scores.mean()




#______________________________________________________________________________
# Leave One per Group out and calc user_features new
# no pre implemented validator found - Predefined possible split possible
# get uniqueUserIds, search for (first) occurence in df use those for Test Set - Rest for Train set#
# lpo = LeavePOut(ratings['userId'].nunique())
# groups = ratings['userId']
# for train_index, test_index in lpo.split(X=X,y=y, groups=groups):
#    print("TRAIN:", train_index, "TEST:", test_index)
#    X_train, X_test = X.iloc[train_index], X.iloc[test_index]
#    y_train, y_test = y.iloc[train_index], y.iloc[test_index]
#    print(X_train, X_test, y_train, y_test)


# scores = cross_val_score(clf, X=X, y=y, cv=3, scoring='neg_mean_squared_error',
#                          fit_params={'user_features': None, 'movie_features': movie_features})


# clf.fit(X, y)
#predictions = clf.predict(X)

# # > when predicting with the same model the rmse is 0.8168027437257229
# from sklearn.metrics import mean_squared_error
# metric_score = mean_squared_error(y, predictions)
#
# # > the RMSE when predicting the mean of all ratings is 1.1301299466564019
# mean = y.mean()
# metric_score_to_mean = mean_squared_error(y, np.full(len(predictions), mean))


# gkf = GroupKFold(n_splits=3)
# groups = ratings['userId']
# scoring = 'neg_mean_absolute_error'  # negated - the higher the better
#
#
# def cross_validation(model, scoring):
#     scores = cross_val_score(model, X=X, y=y, scoring=scoring, cv=gkf, groups=groups)
#     return scores
#
#
# def param_search(param_grid, n_iter=10):
#     clf = RandomizedSearchCV(batch_sgd, n_iter=n_iter, param_distributions=param_grid, cv=gkf, scoring=scoring)
#     clf.fit(X, y, groups=groups)
#     store_param_search_results([clf], data_handler)
#     for param_name in param_grid.keys():
#         GridSearch_table_plot(clf, param_name)
#     return clf
#
#
# # Param search
# # run 1
# param_grid = {
#     'alpha': 10.0 ** -np.arange(1, 7),
#     'loss': ['squared_loss', 'huber', 'epsilon_insensitive'],
#     'eta0': 10.0 ** -np.arange(1, 5),
#     'power_t': [0.10, 0.15, 0.2],
#     'penalty': ['l2', 'l1', 'elasticnet'],
#     'learning_rate': ['constant', 'optimal', 'invscaling'],
#     'fit_intercept': [False]
# }
#
# gkf = GroupKFold(n_splits=3)
# # param_grid = {
# #     'alpha': 10.0 ** -np.arange(1, 5),
# #     'eta0': 10.0 ** -np.arange(1, 5),
# #     'power_t': [0.10, 0.15, 0.2],
# #     'loss': ['huber'],
# #     'penalty': ['l1'],
# #     'learning_rate': ['constant', 'invscaling'],
# #     'fit_intercept': [False]
# # }
#
# # scoring = 'neg_mean_absolute_error'  # negated - the higher the better
#
# scoring = 'neg_mean_squared_error'
# clf = param_search(param_grid, n_iter=20)
# results = pd.DataFrame(clf.cv_results_).sort_values(by="rank_test_score")

# Cross_val
# best_params = {
# 'power_t': 0.2,
#  'penalty': 'l1',
#  'loss': 'huber',
#  'learning_rate': 'invscaling',
#  'fit_intercept': False,
#  'eta0': 0.01,
#  'alpha': 0.001}
#
# clf = SGD_Batch(data_handler=data_handler, **best_params)
# scoring = 'neg_mean_squared_error'
#
# results = cross_validation(clf, scoring)

# batch_sgd.fit(ratings, "")
# pred = batch_sgd.predict(ratings)
