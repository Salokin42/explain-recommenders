




from sklearn.model_selection import train_test_split

import logging
import pickle
import os

import scipy as sp
import pandas as pd
from lightfm import LightFM
from lightfm.evaluation import recall_at_k, auc_score, precision_at_k

from src.DataHandler import FeatureDataHandler
from src.FeatureHandler import FeatureHandler

N_MOVIE_GENOME_FEATURES = 300
NUM_THREADS = 2
NUM_COMPONENTS = 30
NUM_EPOCHS = 4
ITEM_ALPHA = 1e-6

# load relevant data:
data_handler = FeatureDataHandler()

# ratings = data_handler.load_ratings_dev()
ratings = data_handler.load_ratings_dev().iloc[0:500]
user_features = data_handler.load_user_features_subset_dev()

movie_features = data_handler.load_movie_features_raw()

ratings = FeatureHandler.drop_ratings_where_movie_no_genome_tag(ratings, movie_features)
user_features = user_features.dropna()
movie_features = movie_features.dropna()

ratings_matrix = FeatureHandler.lightfm_build_user_item_matrix(ratings, positive_rating_border=3.5, sparse=True)
ratings_matrix_df = FeatureHandler.lightfm_build_user_item_matrix(ratings, positive_rating_border=3.5, sparse=False)

movie_features_top_n = FeatureHandler.get_highest_movie_features(movie_features, n=N_MOVIE_GENOME_FEATURES)
movie_feature_matrix = FeatureHandler.lightfm_build_movie_feature_matrix(movie_features_top_n)

from lightfm.data import Dataset

# #________________ custom function, in general implementation has to be copied to Dataset.data.py
# def build_interactions_niko(self, data):
#     """
#     Build an interaction matrix.
#
#     Two matrices will be returned: a (num_users, num_items)
#     COO matrix with interactions, and a (num_users, num_items)
#     matrix with the corresponding interaction weights.
#
#     Parameters
#     ----------
#
#     data: iterable of (user_id, item_id) or (user_id, item_id, weight)
#         An iterable of interactions. The user and item ids will be
#         translated to internal model indices using the mappings
#         constructed during the fit call. If weights are not provided
#         they will be assumed to be 1.0.
#
#     Returns
#     -------
#
#     (interactions, weights): COO matrix, COO matrix
#         Two COO matrices: the interactions matrix
#         and the corresponding weights matrix.
#     """
#
#     interactions = _IncrementalCOOMatrix(self.interactions_shape(), np.int32)
#     weights = _IncrementalCOOMatrix(self.interactions_shape(), np.float32)
#
#     for datum in data:
#         user_idx, item_idx, weight = self._unpack_datum(datum)
#
#         rating = weight
#         if rating == 5:
#             interaction, weight = 1, 1
#         elif rating == 4.5:
#             interaction, weight = 1, 0.9
#         elif rating == 4:
#             interaction, weight = 1, 0.8
#         elif rating == 3.5:
#             interaction, weight = 1, 0.2
#         elif rating == 3:
#             interaction, weight = 1, 0.1
#         elif rating == 2.5:
#             interaction, weight = -1, 0.2
#         elif rating == 2:
#             interaction, weight = -1, 0.8
#         elif rating == 1.5:
#             interaction, weight = -1, 0.85
#         elif rating == 1:
#             interaction, weight = -1, 0.9
#         elif rating == 0.5:
#             interaction, weight = -1, 1
#         else:
#             raise (ValueError('Rating has a wrong value'))
#
#         interactions.append(user_idx, item_idx, interaction)
#         weights.append(user_idx, item_idx, weight)
#
#     return (interactions.tocoo(), weights.tocoo())
#________________
# build dataset
dataset = Dataset(user_identity_features=True, item_identity_features=False)
dataset.fit(ratings['userId'], ratings['movieId'])
dataset.fit_partial(items=movie_features.index, item_features=movie_features_top_n)

# build interactions
ratings_ = ratings.loc[:, ['userId', 'movieId', 'rating']]
(interactions, weights) = dataset.build_interactions_niko(tuple(rating) for rating in ratings_.values)

# build item_feature_matrix
movie_features_dict = movie_features_top_n.to_dict(orient='index')
list_item_features = [tuple([index, features]) for index, features in movie_features_dict.items()]

item_features = dataset.build_item_features(list_item_features, normalize=False)

# test
interaction_df = pd.DataFrame(interactions.toarray())
weights_df = pd.DataFrame(weights.toarray())
item_features_df = pd.DataFrame(item_features.toarray())

# regular run
model = LightFM(loss='logistic',
                item_alpha=ITEM_ALPHA,
                no_components=NUM_COMPONENTS)

model = model.fit(interactions, epochs=NUM_EPOCHS, num_threads=NUM_THREADS, sample_weight=weights,
                  item_features=item_features)

#save model and data misusing the function
data_handler.save_model(model, 'lightFM_dev.joblib')
data_handler.save_model(movie_features_top_n, 'movie_features_top_n.joblib')
data_handler.save_model(dataset, 'dataset.joblib')
# recall_at_k(model, interactions, k=50) with sample weight
# Out[54]: array([0.19354839, 0.09090909, 0.0412844 ])
# recall_at_k(model, interactions, k=50) without sample weight
# array([0.19758065, 0.12121212, 0.05504587])

# recall mean at 50
# without item_features 0.1423
# with 100 item features 0.012
# with 500 item features  0.019
# recall = recall_at_k(model, interactions, k=500, item_features=item_features)
# print(recall.mean())
#

# logging.info('Calculating AUC Score')
# train_auc = auc_score(model, interactions, item_features=item_features).mean()
# print(train_auc)

# auc withouth item 0.79
# auc with item: 0.58

# grid search

#
# from scipy import stats
# from sklearn.metrics import roc_auc_score
# from sklearn.model_selection import KFold, RandomizedSearchCV
# import numpy as np
#
# # Set distributions for hyperparameters
# randint = stats.randint(low=1, high=65)
# randint.random_state = 42
# gamma = stats.gamma(a=1.2, loc=0, scale=0.13)
# gamma.random_state = 42
# distr = {"no_components": randint, "learning_rate": gamma}
#
#
# # Custom score function
# def scorer(est, x, y=None):
#     return precision_at_k(est, x).mean()
#
#
# # Dummy custom CV to ensure shape preservation.
# class CV(KFold):
#     def split(self, X, y=None, groups=None):
#         idx = np.arange(X.shape[0])
#         for _ in range(self.n_splits):
#             yield idx, idx
#
#
# cv = CV(n_splits=3, random_state=42)
# search = RandomizedSearchCV(
#     estimator=model,
#     param_distributions=distr,
#     n_iter=2,
#     scoring=scorer,
#     random_state=42,
#     cv=cv,
# )
#


# ___________________________

import lime
import numpy as np

import lime.lime_tabular
from src.FeatureHandler import FeatureHandler

training_data_representative = movie_features_top_n  # should hold enough variation for lime to learn
feature_names = FeatureHandler.replace_feature_descriptions(training_data_representative.columns.tolist(), data_handler)

explainer = lime.lime_tabular.LimeTabularExplainer(training_data_representative.values, mode='regression',
                                                   feature_names=feature_names,
                                                   random_state=42,
                                                   discretize_continuous=True)
# IDs as in Dataframe
EXTERNAL_MOVIEID = 1
EXTERNAL_USERID = 761


def predict_lightfm_for_lime(item_features_input):
    """
    Function to wrap lightfms prediction function. For Lime. For Variation of UserFeatures
    :param item_features_input: list of np. arrays of movie features
    :return: Predictions for the different item_features
    """
    predictions = np.empty(0)
    userId_lightFM = dataset._user_id_mapping[EXTERNAL_USERID]
    movieId_lightFM = dataset._item_id_mapping[EXTERNAL_MOVIEID]

    print(type(item_features_input))

    def predict_single_item_features(single_item_features):  # predict for a single feature array
        # build item_feature_matrix
        item_feature_list = [tuple([EXTERNAL_MOVIEID, item_feature]) for item_feature in single_item_features]
        cur_item_feature = dataset.build_item_features(item_feature_list, normalize=False)

        prediction = model.predict(userId_lightFM, np.array([movieId_lightFM]), item_features=cur_item_feature)
        return prediction

    if len(item_features_input == 1):  # when predicting single array - first run of Lime
        prediction = predict_single_item_features(item_features_input)
        predictions = np.concatenate((predictions, prediction))
    else:  # when input is list of features
        for single_item_features in item_features_input:
            prediction = predict_single_item_features(single_item_features)
            predictions = np.concatenate((predictions, prediction))

    return predictions


# build actual movie_features

actual_movie_feature = movie_features_top_n.loc[EXTERNAL_MOVIEID]
explanation = explainer.explain_instance(actual_movie_feature.values, predict_lightfm_for_lime,
                                         num_features=10)
explanation.as_pyplot_figure().show()
