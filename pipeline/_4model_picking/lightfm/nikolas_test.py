import numpy as np
import pandas as pd
import scipy
from sklearn.model_selection import train_test_split

from src.DataHandler import FeatureDataHandler
from src.FeatureHandler import FeatureHandler

# load relevant data:
data_handler = FeatureDataHandler()


ratings = data_handler.load_ratings_dev()
user_features = data_handler.load_user_features_dev_raw()
movie_features = data_handler.load_movie_features_raw()

ratings = FeatureHandler.drop_ratings_where_movie_no_genome_tag(ratings, movie_features)
user_features = user_features.dropna()
movie_features = movie_features.dropna()

ratings_train, ratings_test = train_test_split(ratings, test_size=0.33, random_state=42)
ratings = [ratings_train, ratings_test]

# build interaction matrix
min_rating = 4
ratings_ = [FeatureHandler.build_user_item_matrix(rating) for rating in ratings]
ratings_ = [rating.applymap(lambda rating: rating if rating >= min_rating else 0) for rating in ratings_]
ratings_matrix = [scipy.sparse.csr_matrix(rating.values) for rating in ratings_]

# modify movie_features
movie_features_df = [movie_features.loc[rating.index] for rating in ratings_]  # get train and test movie features
movie_features_matrix = [scipy.sparse.csr_matrix(movie_feature) for movie_feature in movie_features_df]

# sanity check
ratings_matrix[0].shape[0] == movie_features_matrix[0].shape[0]
ratings_matrix[1].shape[0] == movie_features_matrix[1].shape[0]

# Fit Light FM only on interaction matrix
from lightfm import LightFM

NUM_THREADS = 2
NUM_COMPONENTS = 30
NUM_EPOCHS = 3
ITEM_ALPHA = 1e-6

model = LightFM(loss='warp',
                item_alpha=ITEM_ALPHA,
                no_components=NUM_COMPONENTS)
# model = model.fit(ratings_matrix[0].transpose(), epochs=NUM_EPOCHS, num_threads=NUM_THREADS)

model = model.fit(ratings_matrix[0].transpose(), epochs=NUM_EPOCHS, num_threads=NUM_THREADS,
                  item_features=movie_features_matrix[0])

from lightfm.evaluation import auc_score

train_auc = auc_score(model, ratings_matrix[0].transpose(), num_threads=NUM_THREADS,
                      item_features=movie_features_matrix[0]).mean()
print("AUC on training data is {auc}".format(auc=train_auc))
# AUC on training data is 0.9252465963363647
# TODO: Uses the internal ID, not the actual one
model.predict(1, np.arange(0, 1))

# TODO Does not work because users are different than in fitted model
# Possible solutions: Split test_train data per Groups and leave one rating out per person
# already implemented in _4.scikit.regression_cla.py

# train_auc = auc_score(model, ratings_matrix[1], train_interactions=ratings_matrix[0],
#                       num_threads=NUM_THREADS,
#                       item_features=movie_features_matrix[0],
#                       check_intersections=False).mean()
#
#
# print("AUC on test data is {auc}".format(auc=train_auc))


# chech sensitivity of model for item_features
movieIds = [1]
movie_features_sensitivity = movie_features_df[0]
movie_features_sensitivity = movie_features_sensitivity.loc[movieIds]
movie_features_sensitivity = scipy.sparse.csr_matrix(movie_features_sensitivity)

model.predict(761, np.arange(0, 1), item_features=movie_features_sensitivity)
# results regular model.predict(761,np.arange(0,1),item_features=movie_features_sensitivity) = array([-1.20548618])
# with movie_features_df *100 > array([-70.57978821])
# >>> When fitted and not cold-start, still sensitive to features


# Test if LIME is working
# try to explain instance
model.predict(761, np.arange(0, 1), item_features=movie_features_sensitivity)


# predict capsule
def predict_lightfm_for_lime(item_features):
    predictions = np.empty(0)

    for item_feature in item_features:
        item_feature_sparse = scipy.sparse.csr_matrix(item_feature).transpose()

        prediction = model.predict(1, np.arange(0, 1), item_features=item_feature_sparse)
        predictions = np.concatenate((predictions, prediction))

    return predictions

# predict_function > change movie_features_sensitivity

import lime
import lime.lime_tabular
from src.FeatureHandler import FeatureHandler

training_data_representative = movie_features.values  # should hold enough variation for lime to learn
feature_names = FeatureHandler.replace_feature_descriptions(training_data_representative.columns.tolist(), data_handler)

explainer = lime.lime_tabular.LimeTabularExplainer(training_data_representative, mode='regression',
                                                   feature_names=feature_names,
                                                   random_state=42,
                                                   discretize_continuous=True)

movieIds = [1]
actual_movie_feature = movie_features_df[0].loc[movieIds].iloc[0]
explanation = explainer.explain_instance(actual_movie_feature.values.transpose(), predict_lightfm_for_lime,
                                         num_features=10)
explanation.as_pyplot_figure().show()
