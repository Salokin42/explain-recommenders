from src.DataHandler import FeatureDataHandler
from src.FeatureHandler import FeatureHandler

# load relevant data:
data_handler = FeatureDataHandler()

ratings = data_handler.load_ratings_dev()
movie_features = data_handler.load_movie_features_raw()
user_features = data_handler.load_user_features_dev_raw()  # müssen wenn überhaupt benötigt noch auf [0,1] skaiert werden, z.B. mit sklean MinMaxScaler; würde erstmal nur die movie_features als Metadaten einfließen lassen

# build interaction matrix
interaction_matrix = FeatureHandler.build_movie_user_matrix(ratings)
