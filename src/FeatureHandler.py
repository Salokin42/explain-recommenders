import logging

import scipy as sp
import numpy as np
import pandas as pd

from sklearn.base import TransformerMixin
from sklearn.preprocessing import MinMaxScaler


class FeatureHandler(TransformerMixin):
    def __init__(self, ):
        self.movie_features = None
        self.user_features = None

    def fit(self, movie_features, user_features, *_):
        self.movie_features = movie_features
        self.user_features = user_features
        return self

    def append_user_features(self, user_features):
        self.user_features = self.user_features.append(user_features)

    def transform(self, X, *_):
        # To-Do Think about better solution; Should return single series for Explainer
        def build_combined_features(X):
            return X.apply(
                lambda x: self.movie_features.loc[x['movieId']] * self.user_features.loc[x['userId']].values, axis=1)

        if isinstance(X, pd.Series):
            X = pd.DataFrame(X).transpose()
            combined_features = build_combined_features(X)
            return combined_features.iloc[0]

        else:
            combined_features =build_combined_features(X)
            return combined_features

    def transform_given_movie_features(self, X, movie_features_array):
        """

        :param X: a df or series with single (userId,movieID)
        :param movie_features_array: array of movie_features
        :return: interaction features > multiplication of movie_features with responding user_features
        """
        userId = X['userId']
        user_features = self.user_features.loc[userId]
        movie_features_input = pd.DataFrame(movie_features_array)

        result = movie_features_input * user_features.values
        return result

    def transform_given_user_features(self, X, user_features_array):
        """

        :param X: a df or series with single (userId,movieID)
        :param movie_features_array: array of movie_features
        :return: interaction features > multiplication of movie_features with responding user_features
        """
        movieId=X['movieId']
        movie_features =self.movie_features.loc[movieId]
        user_features_input = pd.DataFrame(user_features_array)

        result = user_features_input * movie_features.values
        return result



    @staticmethod
    def get_highest_movie_features(movie_features, n=100):
        """

        :param movie_features: complete set of movie features
        :return: complete set of movie features with only n most important genome_tag_features
        """
        sort_out = {'464': 'great', '445': 'good', '1072': 'very good', '364': 'excellent'}
        movie_features.drop(sort_out.keys(), axis=1, inplace=True)

        movie_features_no_genres_mean = movie_features.loc[:, "1":].mean().sort_values(ascending=False)

        genre_columns = movie_features.loc[:, :'Western'].columns.tolist()  # always use genres
        genome_feature_columns = movie_features_no_genres_mean.index[:n].tolist()
        subset_columns = genre_columns + genome_feature_columns

        return movie_features[subset_columns]

    @staticmethod
    def build_user_features(X, y, movie_features, add_avg_user_rating=False):
        """

        :param X: df(userId,movieId) - Must be in same order as y
        :param y: rating - Must be in same order as ratings
        :param movie_features:
        :param _:
        :return: user_features for given userIds weighted by rating and scaled to [0,1]
        """
        logging.info('Creating User Features ')
        if add_avg_user_rating:  X['avg_rating'] = 1
        user_features = X.merge(movie_features, how='inner', on='movieId').drop('movieId',
                                                                                axis=1)  # join_movie_features
        user_features.iloc[:, 1:] = user_features.iloc[:, 1:].multiply(y.reset_index(drop=True),
                                                                       axis=0)  # weight with rating, ignore index
        user_features = user_features.groupby('userId').mean() / 5  # calculate the mean and scale to [0:1]
        user_features.rename(columns=lambda x: "user_" + x, inplace=True)

        return user_features

        # Alternative for scaling - but if used should be reimplemented on instead loading a scaler that has
        # been fitted on all ratings, not just the inputs ones of this method.
        # user_features = pd.DataFrame(MinMaxScaler(feature_range=(0, 1)).fit_transform(user_features),
        #                   index=user_features.index,
        #                   columns=user_features.columns)

    @staticmethod
    def build_user_item_matrix(ratings):
        return ratings.pivot(index='movieId', columns='userId', values='rating')

    @staticmethod
    def lightfm_build_user_item_matrix(ratings, positive_rating_border=3.5, sparse=True):
        """

        :param ratings:
        :param positive_rating_border: incluse, everything with this value and above is rated as positive interaction,
        everything below as negative
        :return: Matrix encoded with -1 for negative interaction, +1 for positive and 0 if there was none
        """
        matrix = ratings.pivot(index='movieId', columns='userId', values='rating')
        matrix.fillna(0, inplace=True)

        def set_rating(rating):
            if rating == 0:
                return 0
            elif rating < positive_rating_border:
                return -1
            elif rating >= positive_rating_border:
                return 1

        matrix = matrix.applymap(set_rating)

        if sparse:
            return sp.sparse.csr_matrix(matrix)

        return matrix

    @staticmethod
    def lightfm_build_movie_feature_matrix(movie_features, sparse=True):
        eye = sp.eye(movie_features.shape[0], movie_features.shape[0])
        item_features = sp.hstack((eye, movie_features))

        if sparse:
            return sp.sparse.csr_matrix(item_features)
        return item_features

    @staticmethod
    def drop_ratings_where_movie_no_genome_tag(ratings, movie_features):
        """

                :param ratings:
                :param movie_features: movie_features vector, can include nas
                :return: ratings that have genome feature number 1228
                """
        movie_ids = movie_features[movie_features['1101'].notnull()].index
        ratings = ratings[ratings['movieId'].isin(movie_ids)]
        return ratings

    @staticmethod
    def get_most_popular_movies(ratings, n=50):
        most_popular_movies = ratings.loc[:, ['movieId']].groupby(['movieId']).size().sort_values(ascending=False)
        return most_popular_movies.index[0:n]

    @staticmethod
    def build_genome_tags_dict(data_handler):
        # Build Lookup dict for genome tags
        genome_tags = data_handler.load_raw_genome_tags()
        genome_tags_dict = genome_tags.set_index('tagId').iloc[:, 0].to_dict()
        genome_tags_dict = {str(key): str(value) for key, value in
                            genome_tags_dict.items()}  # workaround for pandas index
    @staticmethod
    def build_genome_tags_dict_standard(data_handler):
        genome_tags = data_handler.load_genome_tags_german()
        return genome_tags.to_dict()

    @staticmethod
    def replace_feature_descriptions(feature_names, data_handler, genome_tags='basic_german'):
        """

        :param feature_names: iterable of features
        :param data_handler: data_handler
        :return: list of features with replaced genome_tag names
        """
        if genome_tags == 'basic_german':
            genome_tags_dict = FeatureHandler.build_genome_tags_dict_standard(data_handler)
            genome_tags_dict = {str(tagId): tag for tagId, tag in genome_tags_dict.items()}  # cast keys to string
        else:
            genome_tags_dict = FeatureHandler.build_genome_tags_dict(data_handler)

        if feature_names[0].startswith('user_'):
            feature_names = [feature_name.replace('user_', '') for feature_name in feature_names]

        feature_names_replaced = []
        for feature_name in feature_names:
            if feature_name in genome_tags_dict:
                feature_names_replaced.append(genome_tags_dict[feature_name])
            else:  # assume that it is Genre instead
                feature_names_replaced.append('Genre: ' + feature_name)

        return feature_names_replaced



# @staticmethod
# #Method should be somewhere else
# def get_representative_training_data(data_handler):
#     logging.info('Building representative training data')
#     from src.DataHandler import * DataHandler
#     data_handler = DataHandler()
#     ratings = data_handler.load_
