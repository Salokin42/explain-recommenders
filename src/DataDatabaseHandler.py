import sqlite3
import os
import pandas as pd
import csv

from DataHandler import DataHandler


class DataDatabaseHandler(DataHandler):

    def __init__(self, dataset='ml-20m'):
        super().__init__(dataset)
        self.dbfile = os.path.join(self.path_processed, 'movielens.sqlite')
        self.con = sqlite3.connect(self.dbfile)
        self.cur = self.con.cursor()

    def save_as_csv(self, strSql, filename):
        file = os.path.join(self.path_processed, filename + '.csv')
        self.cur.execute(strSql)

        with open(file, 'w') as out_csv_file:
            csv_out = csv.writer(out_csv_file)
            # write header
            csv_out.writerow([d[0] for d in self.cur.description])
            # write data
            for result in self.cur:
                csv_out.writerow(result)

    def db_load_table(self, table_name):
        strSql = "SELECT * FROM {}".format(table_name)
        features = pd.read_sql(strSql, con=self.con)
        return features

    def db_read_user_features(self):
        # ~1.274 GB RAM when from files
        return self._read_table_from_db('main_user_features')

    def db_read_movie_features(self):
        # ~251MB RAM
        return self._read_table_from_db('movies')



