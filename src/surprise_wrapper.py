# Functions for scykit-learn_surprise

import os
import pandas as pd

from surprise.prediction_algorithms import *
from surprise.model_selection import KFold
from surprise import accuracy
from surprise import Dataset, Reader

from src.ResultHandler import *

# https://surprise.readthedocs.io/en/stable/prediction_algorithms_package.html
def _surprise_path():
    return os.path.join(os.getcwd(), 'pipeline', '4_model_picking', 'surprise')


def build_surprise_data(pandas_df_ratings):
    ratings = pandas_df_ratings.drop(columns='timestamp')
    reader = Reader(rating_scale=(1, 5))
    return Dataset.load_from_df(ratings, reader)

def supr_build_ratings_trainset_from_df(ratings_df):
    ratings = build_surprise_data(ratings_df)
    return ratings.build_full_trainset()

def build_surpirse_folds(train):
    reader = Reader(rating_scale=(1, 5))


def init_all_models(sim_options={'user_based': True}):
    return {
        'BaselineOnly': BaselineOnly(),
        'NormalPredictor': NormalPredictor(),
        'KNNBasic': KNNBasic(sim_options=sim_options),
        'KNNWithMeans': KNNWithMeans(sim_options=sim_options),
        'KNNWithZScore': KNNWithZScore(sim_options=sim_options),
        'KNNBaseline': KNNBaseline(sim_options=sim_options),
        'SVD': SVD(),
        'SVDpp': SVDpp(),
        'NMF': NMF(), 'SlopeOne': SlopeOne(), 'CoClustering': CoClustering()
    }


def init_all_models_and_std_params():
    models = init_all_models()
    sim_options = {'user_based': False}
    models2 = {'KNNBasic_user_based_false': KNNBasic(sim_options=sim_options),
               'KNNWithMeans_user_based_false': KNNWithMeans(sim_options=sim_options),
               'KNNWithZScore_user_based_false': KNNWithZScore(sim_options=sim_options),
               'KNNBaseline_user_based_false': KNNBaseline(sim_options=sim_options)}
    return {**models, **models2}


def evaluate_model(datasubset_name, data, model, validation_method=KFold(n_splits=2), parameters="", ):
    """
    Genreral method for evaluation the surprise algorithms
    :param datasubset_name:
    :param data:
    :param model:
    :param validation_method:
    :param no_splits:
    :param parameters:
    :return:
    """
    print("Computing model: " + str(model.__class__))
    df = pd.DataFrame(columns=['RMSE', 'MAE'])

    best_rmse = 999
    best_model = None

    kf = validation_method

    for trainset, testset in kf.split(data):
        model.fit(trainset)
        predictions = model.test(testset)
        rmse, mae = accuracy.rmse(predictions), accuracy.mae(predictions)
        df.loc[len(df)] = [rmse, mae]
        if rmse < best_rmse:
            best_rmse = rmse
            best_model = model

    df = df.agg(['mean', 'std'])
    # To-Do: solve name problem
    return Result(datasubset_name, model.__class__, df.loc['mean', 'RMSE'], df.loc['std', 'RMSE'],
                  df.loc['mean', 'MAE'],
                  df.loc['std', 'MAE'], parameters=parameters, model=best_model, validation_method=validation_method)

# def models_sanity_check(data, description='results_surprise_sanity', models=None):
#     if models is None:
#         models = init_all_models()
#         del models['SVDpp']
#     kf = KFold(n_splits=2)
#
#     results_list = []
#     for model_name, model in models.items():
#
#         print("Computing model " + model_name)
#         for trainset, testset in kf.split(data):
#             model.fit(trainset)
#             predictions = model.test(trainset.build_testset())
#             results_list.append((model_name, accuracy.rmse(predictions), accuracy.mae(predictions)))
#     results = _results_list_to_df(results_list)
