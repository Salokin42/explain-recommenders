import urllib.request
import json
import pandas as pd

from src.DataHandler import FeatureDataHandler


class TheMovieDBConnector():
    API_KEY = '8d135f10f44bf6f89989b5e5c489a160'

    # examlple for URL
    # https://api.themoviedb.org/3/movie/862?api_key=8d135f10f44bf6f89989b5e5c489a160&language=de-DE&region=DE
    API_KEY = '8d135f10f44bf6f89989b5e5c489a160'

    def __init__(self, data_handler):
        self.data_handler = data_handler

    def get_tmdbIds(self, movieIds):
        links = self.data_handler.load_links()
        links.set_index('movieId', inplace=True)

        ids = links.loc[movieIds, 'tmdbId']
        ids.fillna(-999, inplace=True)  # set to "wrong" value > request later will not work

        ids = ids.astype(int)

        return ids.to_dict()

    def _build_request_url(self, tmdbId):
        url = 'https://api.themoviedb.org/3/movie/{tmbId}?api_key={key}&language=de-DE&region=DE'.format(tmbId=tmdbId,
                                                                                                         key=self.API_KEY)
        return url

    def _build_request_urls(self, tmdbIds):
        urls = {}
        for movieId, tmdbId in tmdbIds.items():
            urls[movieId] = self._build_request_url(tmdbId)
        return urls

    def _get_request_overview(self, request_url):

        # handle timeout error
        try:
            with urllib.request.urlopen(request_url) as response:
                response_json = response.read()
            movie_information = json.loads(response_json)

            return movie_information['overview']

        except urllib.error.HTTPError as error:
            return 'Keine Filmbeschreibung vorhanden'
        except:
            return 'Keine Filmbeschreibung vorhanden'

    def _get_request_overviews(self, request_urls_dict):
        movie_descriptions_dict = {}

        for movieId, url in request_urls_dict.items():
            movie_descriptions_dict[movieId] = self._get_request_overview(url)

        return movie_descriptions_dict

    def get_movie_descriptions(self, movieIds):

        ids_dict = self.get_tmdbIds(movieIds)
        request_urls_dict = self._build_request_urls(ids_dict)
        movie_descriptions_dict = self._get_request_overviews(request_urls_dict)

        return pd.Series(movie_descriptions_dict)

    # Build the api url  manually
    # beschreibung ist dann im json object in overview
    #
    # def _build_request_url(tmbId):
    #     url = 'https://api.themoviedb.org/3/movie/{tmbId}?api_key={key}&language=de-DE&region=DE'.format(tmbId=tmbId,
    #                                                                                                      key=API_KEY)
    #     return url
    #
    #
    # url = _build_request_url(tmdbId)
    #
    # with urllib.request.urlopen(url) as response:
    #     response_json = response.read()
    #
    # movie_information = json.loads(response_json)
    # overview = movie_information['overview']
