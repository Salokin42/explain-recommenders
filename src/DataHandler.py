import os
import io
import logging
import time
import zipfile
import pickle
import json
from joblib import load
from pathlib import Path
import numpy as np

from six.moves import urllib

import pandas as pd
from sklearn.model_selection import train_test_split


# call fetch_ml data, then setup_data
class DataHandler:
    def __init__(self, dataset='ml-20m'):
        """ use ml-20m or ml-latest-small"""
        self.dataset = dataset
        self.path_data = os.path.join(os.path.dirname(os.path.dirname(__file__)), "data")
        self.path_raw = os.path.join(os.path.dirname(os.path.dirname(__file__)), "data", "raw", self.dataset)
        self.path_imdb = os.path.join(os.path.dirname(os.path.dirname(__file__)), "data", "raw", "imdb")
        self.path_processed = os.path.join(os.path.dirname(os.path.dirname(__file__)), "data", "processed",
                                           self.dataset)
        self.path_model = os.path.join(os.path.dirname(os.path.dirname(__file__)), "model")
        self.path_evaluation = os.path.join(os.path.dirname(os.path.dirname(__file__)), "data", "evaluation")
        self.download_link = self._find_download_link()

        self.path_thesis = Path('/media/sf_PycharmProjects_Windows/Bilder_MT')

    def _find_download_link(self):
        if self.dataset == "ml-20m":
            # TODO check if correct data - is it the recommended 27m for new research?
            return "http://files.grouplens.org/datasets/movielens/ml-20m.zip"
        elif self.dataset == "ml-latest-small":
            return "http://files.grouplens.org/datasets/movielens/ml-latest-small.zip"
        else:
            return ""

    def setup_data_folders(self):
        paths = [self.path_data, self.path_raw, self.path_processed]
        for path in paths:
            if not os.path.isdir(path):
                os.makedirs(path)

    def fetch_ml_data(self):
        zip_file_path = os.path.join(self.path_raw, "..", self.dataset + ".zip")
        urllib.request.urlretrieve(self.download_link, zip_file_path)
        print('Downloaded {}, now extracting'.format(self.dataset))
        zip_file = zipfile.ZipFile(zip_file_path)
        zip_file.extractall(os.path.dirname(zip_file.filename))
        time.sleep(0.5)

    def setup_data(self):
        self._setup_ratings_training_test()
        self._setup_data_remaining()
        self._build_userId_train_index()
        self._setup_ratings_try()

    def _setup_ratings_training_test(self):
        # possibly better to split according to number of ratings per user?
        ratings = pd.read_csv(os.path.join(self.path_raw, "ratings.csv"))

        train_set, test_set = train_test_split(ratings, test_size=0.2, random_state=42)

        test_path = os.path.join(self.path_processed, "test")
        for path in (self.path_processed, test_path):
            if not os.path.isdir(path):
                os.makedirs(path)

        ratings.to_pickle(os.path.join(self.path_processed, "ratings_all.pkl"))
        train_set.to_pickle(os.path.join(self.path_processed, "ratings_train.pkl"))
        test_set.to_pickle(os.path.join(test_path, "ratings_test.pkl"))

        ratings.to_csv(os.path.join(self.path_processed, "ratings_all.csv"))
        train_set.to_csv(os.path.join(self.path_processed, "ratings_train.csv"))
        test_set.to_csv(os.path.join(test_path, "ratings_test.csv"))

    def _setup_ratings_try(self):
        """
        builds the ratings_try.pkl that consists around 1.000.000 ratings
        """
        userId_indexes = self.load_userId_train_index()

        build_indexes = []
        for indexes in userId_indexes.values():
            if len(build_indexes) >= 1000000:
                break
            build_indexes.extend(indexes)

        build_indexes.sort()
        train_set = self.load_ratings_train()
        try_set = train_set.loc[build_indexes]

        try_set.to_pickle(os.path.join(self.path_processed, "ratings_try.pkl"))
        # old random split method
        #
        # train_set = self.load_ratings_train()
        # _, try_set = train_test_split(train_set, test_size=0.05)
        # try_set.to_pickle(os.path.join(self.path_processed, "ratings_try.pkl"))

    def _setup_data_remaining(self):
        movies = pd.read_csv(os.path.join(self.path_raw, "movies.csv"))
        movies.to_pickle(os.path.join(self.path_processed, "movies.pkl"))

    def _build_userId_train_index(self):
        """
        Builds a dict
        :return: dict of form userId, List(indexes of ratings)
        """
        train_set = self.load_ratings_train()
        uniqueUseriDs = train_set['userId'].unique()

        userId_indexes = {}
        for uniqueUseriD, i in zip(uniqueUseriDs, range(len(uniqueUseriDs))):
            if i % 1000 == 0:
                print('Building User Index No. : {}'.format(i))

            # userId_indexes[uniqueUseriD] = train_set[train_set['userId'] == uniqueUseriD]['userId'].index.values.astype(
            #     int)
            userId_indexes[uniqueUseriD] = train_set[train_set['userId'] == uniqueUseriD][
                'userId'].index.values.tolist()

        path = os.path.join(self.path_processed, 'ratings_userId_ratings_dict.pkl')
        with open(path, 'wb') as handle:
            pickle.dump(userId_indexes, handle)
        return userId_indexes

    @staticmethod
    def deprcated_build_movie_user_matrix(ratings_data):
        """
        :param ratings_data: pandas dataframe of ratings data
        :return: movie_user_matrix
        """
        return ratings_data.pivot(index='movieId', columns='userId', values='rating')

    def get_most_popular_movies(self, n=50):
        logging.warning('Deprecated Please use FeatureDataHandler get_most_popular movies instead of Data Handler one')
        ratings = self.load_ratings_all()
        most_popular_movies = ratings.loc[:, ['movieId']].groupby(['movieId']).size().sort_values(ascending=False)
        return most_popular_movies.index[0:n]

    # Moved to Feature Handler
    # def drop_genome_na_ratings(self, ratings, movie_features):
    #     """
    #
    #     :param ratings:
    #     :param movie_features: movie_features vector, can include nas
    #     :return: ratings that have genome feature number 1228
    #     """
    #     movie_ids = movie_features[movie_features['1128'].notnull()].index
    #     ratings = ratings[ratings['movieId'].isin(movie_ids)]
    #     return ratings

    @staticmethod
    def add_ratings(ratings, new_ratings):
        """
        :param ratings: iterable with userId,movieId,rating(,timestamp)
        :return: pd.df
        """
        new_ratings = pd.DataFrame(new_ratings, columns=['userId', 'movieId', 'rating', 'timestamp'])
        return ratings.append(new_ratings, ignore_index=True)

    def test_size(self):
        # check if correct sizes
        # should be full, try, train, test
        ratings = pd.read_csv(os.path.join(self.path_raw, "ratings.csv"))

        try_set = pd.read_pickle(os.path.join(self.path_processed, "ratings_try.pkl"))
        train_set = pd.read_pickle(os.path.join(self.path_processed, "ratings_train.pkl"))
        test_set = pd.read_pickle(os.path.join(self.path_processed, "test", "ratings_test.pkl"))

        lengths = []
        for data in (ratings, try_set, train_set, test_set):
            lengths.append(len(data))
        print("should be full, try, train, test")
        print(lengths)

    def save_processed_data_as_pkl(self, data, filename):
        data.to_pickle(os.path.join(self.path_processed, filename))

    def save_processed_data_as_csv(self, data, filename, index=False):
        data.to_csv(os.path.join(self.path_processed, filename), index=index)

    def save_movies_metadata(self, movies_metadata):
        self.save_processed_data_as_pkl(movies_metadata, 'movies_with_metadata.pkl')

    def load_pickle_data(self, filename):
        return pd.read_pickle(os.path.join(self.path_processed, filename))

    def load_csv_data(self, filename, index_col=None):
        return pd.read_csv(os.path.join(self.path_processed, filename), index_col=index_col)

    def load_ratings_all(self):
        return self.load_csv_data('ratings_all.csv')

    def load_ratings_dev(self):
        return self.load_csv_data('ratings_dev.csv')

    def load_ratings_train(self):
        return self.load_csv_data('ratings_train.csv')

    def load_ratings_test(self):
        return self.load_csv_data('ratings_test.csv')

    def load_movie_features(self):
        return self.load_csv_data('movie_features.csv', index_col='movieId')

    def load_user_features(self):
        return self.load_csv_data('user_features.csv', index_col='userId')

    def load_user_features_dev(self):
        return self.load_csv_data('user_features_dev.csv',
                                  index_col='userId')  # file with user_feature for first 500 users

    def load_movie_features_raw(self):
        return self.load_csv_data('movie_features_raw.csv', index_col='movieId')

    def load_movie_features_subset(self):
        return self.load_csv_data('movie_features_subset.csv',
                                  index_col='movieId')  # file with user_feature for first 500 users

    def load_user_features_raw(self):
        return self.load_csv_data('user_features_raw.csv', index_col='userId')

    def load_user_features_dev_raw(self):
        return self.load_csv_data('user_features_dev_raw.csv',
                                  index_col='userId')  # file with user_feature for first 500 users

    def load_user_features_subset_dev(self):
        return self.load_csv_data('user_features_subset_dev.csv',
                                  index_col='userId')  # file with user_feature for first 500 users

    def load_movies(self):
        return pd.read_csv(os.path.join(self.path_raw, 'movies.csv'), index_col='movieId')

    def load_movies_german(self):
        return pd.read_csv(os.path.join(self.path_processed, 'movies_german.csv'), index_col='movieId')

    def load_userId_train_index(self):
        path = os.path.join(self.path_processed, 'ratings_userId_ratings_dict.pkl')

        with open(path, 'rb') as handle:
            userId_indexes = pickle.load(handle)
        return userId_indexes

    def load_raw_tags(self):
        """ use ml-20m or ml-latest-small"""
        if self.dataset == 'ml-20m':
            return pd.read_csv(os.path.join(self.path_raw, "tags.csv"))
        else:
            print('Could not load tags Only available for dataset ml-20m ')

    def load_raw_genome_scores(self):
        """ use ml-20m or ml-latest-small"""
        if self.dataset == 'ml-20m':
            return pd.read_csv(os.path.join(self.path_raw, "genome-scores.csv"))
        else:
            print('Could not load raw genome scores. Only available for dataset ml-20m ')

    def load_raw_genome_tags(self):
        """ use ml-20m or ml-latest-small"""
        if self.dataset == 'ml-20m':
            return pd.read_csv(os.path.join(self.path_raw, "genome-tags.csv"))
        else:
            print('Could not load raw genome tags. Only available for dataset ml-20m ')

    def load_genome_tags_german(self):
        return pd.read_csv(os.path.join(self.path_processed, 'genome_tags_german.csv'), index_col='tagId').iloc[:,
               0]  # workaround to get series

    def load_links(self):
        return pd.read_csv(os.path.join(self.path_raw, 'links.csv'))

    def load_title_translations(self):
        return pd.read_csv(os.path.join(self.path_imdb, "title.akas.tsv"), sep='\t')

    def save_model(self, model, filename):
        file = open(os.path.join(self.path_model, filename), 'wb')
        pickle.dump(model, file)

    def load_model(self, filename='SGDBatchRegressor_subset.joblib'):
        return load(os.path.join(self.path_model, filename))

    def load_experiment_data_raw(self, filename):
        return pd.read_excel(os.path.join(self.path_evaluation, filename))

    def save_experiment_data(self, data, filename='experiment.csv'):
        data.to_csv(os.path.join(self.path_evaluation, filename))

    def load_experiment_data(self, filename='experiment.csv'):
        return pd.read_csv(os.path.join(self.path_evaluation, filename), index_col='lfdn')

    def load_experiment_data_complete(self, filename='experiment_complete.csv'):
        return pd.read_csv(os.path.join(self.path_evaluation, filename), index_col='lfdn')

    def load_experiment_recommendations(self):
        return pd.read_csv(os.path.join(self.path_evaluation, 'recommendations.csv'))

    def load_experiment_explanations(self):
        return pd.read_csv(os.path.join(self.path_evaluation, 'explanations.csv'))

    def load_experiment_ratings(self):
        return pd.read_csv(os.path.join(self.path_evaluation, 'ratings.csv'))

    def load_experiment_top_user_features(self):
        return pd.read_csv(os.path.join(self.path_evaluation, 'top_user_features.csv'))

    def write_evaluation_results_text(self, filename, string):
        file = open(os.path.join(self.path_thesis, filename), 'w+')
        file.write(string)
        file.close()



class FeatureDataHandler(DataHandler):

    def get_most_popular_movies(self, n=50):
        if n < 500:
            json_popular_movies = '{"movieId":{"0":296,"1":356,"2":318,"3":593,"4":480,"5":260,"6":110,"7":589,"8":2571,"9":527,"10":1,"11":457,"12":150,"13":780,"14":50,"15":1210,"16":592,"17":1196,"18":2858,"19":32,"20":590,"21":1198,"22":608,"23":47,"24":380,"25":588,"26":377,"27":1270,"28":858,"29":2959,"30":2762,"31":364,"32":344,"33":4993,"34":648,"35":2028,"36":1580,"37":595,"38":500,"39":367,"40":5952,"41":165,"42":597,"43":1240,"44":1136,"45":3578,"46":153,"47":1097,"48":1197,"49":736,"50":34,"51":1721,"52":231,"53":1265,"54":4306,"55":316,"56":7153,"57":733,"58":1291,"59":1214,"60":541,"61":4226,"62":1036,"63":1193,"64":2628,"65":349,"66":587,"67":2716,"68":10,"69":539,"70":586,"71":1073,"72":1704,"73":208,"74":357,"75":1527,"76":1089,"77":253,"78":1221,"79":1200,"80":2997,"81":3793,"82":1617,"83":1213,"84":329,"85":39,"86":292,"87":293,"88":454,"89":6539,"90":924,"91":3996,"92":434,"93":1206,"94":2683,"95":185,"96":1961,"97":1923,"98":2396,"99":111,"100":1682,"101":4973,"102":912,"103":21,"104":6,"105":4963,"106":223,"107":4886,"108":5445,"109":161,"110":288,"111":778,"112":6377,"113":919,"114":1258,"115":5349,"116":2918,"117":2329,"118":2706,"119":750,"120":339,"121":1208,"122":3114,"123":7361,"124":1393,"125":2,"126":1387,"127":95,"128":1517,"129":141,"130":4995,"131":1222,"132":1307,"133":6874,"134":2987,"135":1784,"136":25,"137":2291,"138":1610,"139":2115,"140":1968,"141":1917,"142":3147,"143":19,"144":36,"145":1259,"146":2916,"147":1732,"148":17,"149":300,"150":1080,"151":1225,"152":1246,"153":551,"154":2355,"155":58559,"156":1101,"157":2502,"158":5418,"159":2174,"160":8961,"161":1584,"162":1220,"163":2012,"164":1079,"165":1219,"166":2797,"167":508,"168":4027,"169":2011,"170":62,"171":1356,"172":440,"173":2710,"174":4022,"175":2791,"176":474,"177":788,"178":4878,"179":33794,"180":7438,"181":410,"182":1653,"183":1573,"184":3897,"185":442,"186":11,"187":2324,"188":5989,"189":104,"190":337,"191":923,"192":594,"193":2000,"194":4011,"195":1127,"196":904,"197":16,"198":2700,"199":4896,"200":1391,"201":509,"202":368,"203":1247,"204":2692,"205":6365,"206":1485,"207":2617,"208":353,"209":2054,"210":3751,"211":5378,"212":235,"213":4034,"214":3623,"215":317,"216":266,"217":786,"218":1407,"219":1288,"220":3977,"221":3408,"222":1374,"223":3481,"224":350,"225":1090,"226":3052,"227":6711,"228":3948,"229":8636,"230":1380,"231":908,"232":1370,"233":6333,"234":1304,"235":1394,"236":32587,"237":2194,"238":1230,"239":1625,"240":555,"241":1676,"242":1183,"243":2542,"244":1641,"245":1252,"246":708,"247":2268,"248":553,"249":2640,"250":1278,"251":1148,"252":173,"253":160,"254":420,"255":1234,"256":1954,"257":1207,"258":2712,"259":8360,"260":3527,"261":1544,"262":3949,"263":1201,"264":1500,"265":5816,"266":520,"267":920,"268":784,"269":44191,"270":832,"271":163,"272":48516,"273":2081,"274":370,"275":2353,"276":497,"277":653,"278":1608,"279":2985,"280":3471,"281":1266,"282":1552,"283":1028,"284":903,"285":953,"286":1035,"287":79132,"288":1199,"289":2302,"290":3175,"291":1639,"292":435,"293":2699,"294":2395,"295":2599,"296":196,"297":2321,"298":1233,"299":485,"300":3176,"301":3994,"302":2406,"303":466,"304":1673,"305":2657,"306":5618,"307":1358,"308":8665,"309":1320,"310":1777,"311":1997,"312":3081,"313":8368,"314":3253,"315":1376,"316":2001,"317":48,"318":2006,"319":7,"320":3753,"321":6016,"322":2804,"323":1203,"324":552,"325":1377,"326":432,"327":596,"328":3,"329":7147,"330":70,"331":172,"332":2529,"333":225,"334":158,"335":151,"336":2671,"337":1250,"338":802,"339":5060,"340":4979,"341":1215,"342":1408,"343":1396,"344":1204,"345":543,"346":1094,"347":49272,"348":33493,"349":2470,"350":6934,"351":5669,"352":1302,"353":1242,"354":3418,"355":236,"356":494,"357":60069,"358":6502,"359":5,"360":913,"361":1282,"362":168,"363":745,"364":2890,"365":4246,"366":265,"367":145,"368":59315,"369":1722,"370":112,"371":2763,"372":46578,"373":2100,"374":1748,"375":5218,"376":342,"377":805,"378":3160,"379":8874,"380":1876,"381":2572,"382":2019,"383":4308,"384":1729,"385":58,"386":3448,"387":3033,"388":1909,"389":1645,"390":2167,"391":48394,"392":1276,"393":1275,"394":3421,"395":376,"396":2424,"397":48780,"398":471,"399":282,"400":2947,"401":186,"402":2003,"403":4370,"404":628,"405":315,"406":1674,"407":1333,"408":2701,"409":1088,"410":1092,"411":1285,"412":333,"413":529,"414":1690,"415":910,"416":1262,"417":1372,"418":1375,"419":51662,"420":54286,"421":785,"422":44,"423":56367,"424":2407,"425":515,"426":1747,"427":1466,"428":1263,"429":852,"430":5502,"431":969,"432":3671,"433":4085,"434":5995,"435":4262,"436":7143,"437":246,"438":2005,"439":783,"440":261,"441":1597,"442":345,"443":55820,"444":3911,"445":661,"446":899,"447":4014,"448":1059,"449":5010,"450":5299,"451":2455,"452":2161,"453":2294,"454":2023,"455":2105,"456":252,"457":2080,"458":4720,"459":1293,"460":3255,"461":1339,"462":2278,"463":1569,"464":355,"465":22,"466":673,"467":40815,"468":256,"469":3755,"470":1060,"471":3101,"472":1371,"473":72998,"474":5459,"475":585,"476":1228,"477":914,"478":2770,"479":8644,"480":45722,"481":105,"482":35836,"483":6378,"484":3000,"485":3717,"486":1084,"487":762,"488":5679,"489":902,"490":8784,"491":68157,"492":31,"493":5377,"494":2273,"495":5902,"496":52,"497":4848,"498":68954,"499":1249}}'
            most_popular_movies = pd.read_json(json_popular_movies)
            most_popular_movies = most_popular_movies['movieId']
            return most_popular_movies.iloc[0:n]
        else:
            logging.info("getting most popular movies from file")
            ratings = self.load_ratings_all()
            most_popular_movies = ratings.loc[:, ['movieId']].groupby(['movieId']).size().sort_values(ascending=False)
            most_popular_movies = most_popular_movies['movieId']
            return most_popular_movies.index[0:n]

    def add_movie_information(self, movieIds):
        movies = self.load_movies_german()
        return movies.loc[movieIds]

# data_handler = DataHandler()
# data_handler = FeatureDataHandler()

# data_handler._setup_ratings_training_test()
# data_handler._setup_ratings_training_test()

# data_handler = DataHandler("ml-20m")

# data_handler.fetch_ml_data()
# data_handler.setup_data()
# data_handler.test_size()

# print(data_handler.load_movies().head(5))
# test = data_handler.build_userId_train_index()

# train_set = data_handler.load_ratings_train()
# uniqueUseriDs = train_set['userId'].unique()
#
# userId_indexes = data_handler.build_userId_train_index()
# len(userId_indexes)

# data_handler._build_userId_train_index()
# userId_indexes = data_handler.load_userId_train_index()
# # type(next(iter(userId_indexes.values())))
#
# train_set = data_handler.load_ratings_train()
# data_handler._setup_ratings_try()

# print(userId_indexes)


#### Restrucutre Meta Data
# data_handler = DataHandler("ml-20m")

# user_features_dev = data_handler.load_user_features_dev()
# user_features_dev.drop(labels=['Unnamed: 0'], axis=1, inplace=True)
# data_handler.save_processed_data_as_csv(user_features_dev, 'user_features_dev_norm_proper.csv', index=True)
# print('finished u_dev')
#
# movie_features = data_handler.load_raw_movie_features()
# movie_features.drop(labels=['Unnamed: 0', 'index', ], axis=1, inplace=True)
# data_handler.save_processed_data_as_csv(movie_features, 'movie_features_not_norm_proper.csv', index=True)
# print('finished m')
#
# user_features = data_handler.load_user_features()
# user_features.drop(labels=['Unnamed: 0'], axis=1, inplace=True)
# data_handler.save_processed_data_as_csv(user_features, 'user_features_norm_proper.csv', index=True)
# print('finished u')

####
