import dask.dataframe as dd
import os

from DataHandler import DataHandler


class DataDaskHandler(DataHandler):

    def __init__(self):
        super().__init__('ml-20m')

    def save_to_csv(self, dask_dataframe, filename):
        path = os.path.join(self.path_processed, filename + '.csv')
        dd.to_csv(dask_dataframe, path)

    def create_combined_ratings_feature_file(self):
        user_features = self.load_user_features()
        movie_features = self.load_movie_features()
        ratings_path = os.path.join(self.path_processed, 'ratings_all.csv')
        ratings = dd.read_csv(ratings_path)



        # create csv initally and open

        # for first n ratings:
        #     joined_ratings = join
        # ratings + user_features + movie_features
        # append
        # csv
        # del (joined_ratings)


data_handler = DataDaskHandler()

movie_features = data_handler.load_movie_features()
movie_features.set_index('movieId')

ratings_path = os.path.join(data_handler.path_processed, 'ratings_all.csv')
ratings = dd.read_csv(ratings_path)
ratings.set_index('movieId')

saving_ratings_path = os.path.join(data_handler.path_processed, 'ratings_meta.csv')

#ratings.merge(movie_features, on='movieId').to_csv(saving_ratings_path)
#ratings.join(movie_features, on='movieId').to_csv(saving_ratings_path)
