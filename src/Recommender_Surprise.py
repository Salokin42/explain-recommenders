import pandas as pd
from surprise.prediction_algorithms import KNNBasic

from src.DataHandler import *
from src.surprise_wrapper import *


class Recommender:
    def __init__(self, data_handler=DataHandler('ml-latest-small')):
        """ use ml-20m or ml-latest-small"""
        self.data_handler = data_handler


    def full_recommend_top_n(self, exp_ratings, n=8):
        """
        :param exp_ratings: JSON File of ratings structure
        :param n: number of recommendations to return
        :return:
        """

        def pack_as_json(predictions):
            reformatted_predictions = []

            for prediction in predictions:
                dict = {}
                dict['movieId'] = prediction.iid
                dict['predicted_score'] = prediction.est
                dict['explanations'] = self.get_explanation(prediction.iid, predictions)
                reformatted_predictions.append(dict)

            return json.dumps(reformatted_predictions)

        exp_ratings = self._convert_json_ratings_to_df(exp_ratings)
        top_predictions = self.get_top_n_predictions(exp_ratings, n=n)

        prediction_json = pack_as_json(top_predictions)
        return prediction_json

    def get_top_n_predictions(self, exp_ratings, n=8):
        # FIXME: Takes only the first integer - doesnt check if there are more
        exp_userId = exp_ratings.loc[0, 'userId'].astype(int)
        ratings_trainset = supr_build_ratings_trainset_from_df(self.data_handler.add_ratings(self.ratings, exp_ratings))
        self.algorithm.fit(ratings_trainset)

        unseen_item_ids = self.get_unseen_item_ids(exp_ratings)
        predictions = self.predict_all_unseen_itemIds(exp_userId, unseen_item_ids)
        top_predictions = self.get_best_prediction(predictions, n=n)
        return top_predictions

    def get_explanation(self, movieId, predictions):
        """
        :param movieId:
        :param predictions:
        :return: A List of strings
        """
        # for_knn
        prediction = next((x for x in predictions if x.iid == movieId), None)
        explanations = '%(actual_k)i users who are similar to you have rated the movie with an average of %(est).2f.' % {
            'actual_k': prediction.details['actual_k'], 'est': prediction.est}
        # FIXME: Make two explanations here
        explanation1 = '%(actual_k)i users who are similar to you have rated the movie' % {
            'actual_k': prediction.details['actual_k']}
        explanation2 = 'Their average rating for this movie is %(est).2f.' % {'est': prediction.est}
        explanations = [explanation1, explanation2]
        return explanations

    def get_explanations(self, movieIds, predictions):
        explanations = [self.get_explanation(movieId, predictions) for movieId in movieIds]
        return explanations

    def _convert_json_ratings_to_df(self, user_ratings):
        user_ratings = pd.read_json(user_ratings)
        user_ratings = user_ratings[['userId', 'movieId', 'rating', 'timestamp']]
        return user_ratings

    def get_unseen_item_ids(self, exp_ratings):
        unique_movieIds = self.ratings.loc[:, 'movieId'].unique()
        seen_movieId = exp_ratings.loc[:, 'movieId'].unique()
        return np.delete(unique_movieIds, seen_movieId).tolist()

    def predict_all_unseen_itemIds(self, exp_userId, unseen_itemIds):
        predictions = [self.algorithm.predict(exp_userId, itemId) for itemId in unseen_itemIds]
        return predictions

    def get_best_prediction(self, predictions, n):
        predictions.sort(key=lambda x: x.est, reverse=True)
        return predictions[0:n]
