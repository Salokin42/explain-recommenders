import numpy as np
import pandas as pd
import logging

from sklearn.linear_model import SGDRegressor
from sklearn.base import TransformerMixin
from sklearn.preprocessing import MinMaxScaler

from src.FeatureHandler import FeatureHandler


class SGDBatchRegressor(SGDRegressor):
    def __init__(self, batch_size=10000, loss="squared_loss", penalty="l2", alpha=0.0001,
                 l1_ratio=0.15, fit_intercept=True, max_iter=None, tol=None,
                 shuffle=True, verbose=0, epsilon=0.1,
                 random_state=None, learning_rate="invscaling", eta0=0.01,
                 power_t=0.25, early_stopping=False, validation_fraction=0.1,
                 n_iter_no_change=5, warm_start=False, average=False,
                 n_iter=None):
        super(SGDRegressor, self).__init__(
            loss=loss, penalty=penalty, alpha=alpha, l1_ratio=l1_ratio,
            fit_intercept=fit_intercept, max_iter=max_iter, tol=tol,
            shuffle=shuffle, verbose=verbose, epsilon=epsilon,
            random_state=random_state, learning_rate=learning_rate, eta0=eta0,
            power_t=power_t, early_stopping=early_stopping,
            validation_fraction=validation_fraction,
            n_iter_no_change=n_iter_no_change, warm_start=warm_start,
            average=average, n_iter=n_iter)

        self.batch_size = batch_size
        self.feature_handler = None

    def fit(self, X, y, movie_features, user_features=None):
        """

           :param X: userId, movieId
           :param y: rating
           :param movie_features:
           :param user_features: Can be calculated on the fly, but only if there are ~max. 1000 different userIds.
           :return: model
           """
        logging.info('Fitting model')

        if user_features is None:
            logging.info('Calculating new user_features')
            user_features = FeatureHandler.build_user_features(X, y, movie_features, add_avg_user_rating=False)

        self.feature_handler = FeatureHandler()
        self.feature_handler.fit(movie_features, user_features)

        for pos in range(0, len(X), self.batch_size):
            logging.info("Fitting rating No.:{0}".format(pos))
            X_chunk = X.iloc[pos: pos + self.batch_size]
            X_chunk = self.feature_handler.transform(X_chunk)
            # FIXME might raise an error if Series is not passed until here
            y_chunk = y.iloc[pos: pos + self.batch_size]
            self.partial_fit(X_chunk, y_chunk)
        return self

    def predict(self, X):
        logging.info('Starting prediction')
        predictions = np.empty(0)

        for pos in range(0, len(X), self.batch_size):

            if all(elem in X.columns.tolist() for elem in ['userId', 'movieId']): #check if X hast columns userID and movieId
                logging.info("Predicting rating No.:{0} with added features".format(pos))
                X_chunk = X.iloc[pos: pos + self.batch_size]
                X_chunk = self.feature_handler.transform(X_chunk)

            else:  # for LIME, Interaction Features already joined beforehand
                logging.info("Predicting rating No.:{0} with given features".format(pos))
                X_chunk = X[pos:pos + self.batch_size]

            prediction = super(SGDBatchRegressor, self).predict(X_chunk)  # Call .predict from super class
            predictions = np.concatenate((predictions, prediction))

        return predictions



# data_handler = DataHandler()
# ratings, movie_features, user_features = data_handler.load_ratings_dev(), data_handler.load_movie_features_raw(), data_handler.load_user_features_dev_raw()
# ratings = ratings.iloc[0:100]
# avg_user_rating = user_features['user_avg_rating']
# user_features.drop(labels=['user_avg_rating'], axis=1, inplace=True)
#
# X = ratings.loc[:, ['userId', 'movieId']]
# y = ratings['rating']
# #
# clf = SGDBatchRegressor()
# clf.fit(X=X, y=y, movie_features=movie_features)
#
# predictions = clf.predict(X)
