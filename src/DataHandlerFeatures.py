import os
import pandas as pd
import logging

logging.basicConfig(level=logging.DEBUG)

from DataHandler import DataHandler


class DataHandlerFeatures(DataHandler):
    def __init__(self, ratings_type='dev', dataset='ml-20m'):
        """

        :param ratings_type: dev, test or train
        :param dataset:
        """
        super().__init__(dataset)
        self.ratings_type = ratings_type
        self.ratings = self._set_ratings()
        self.movie_features = self.load_movie_features()

        if ratings_type == 'dev':
            self.user_features = self.load_user_features_dev()
        else:
            self.user_features = self.load_user_features()

    def _set_ratings(self):
        if self.ratings_type == 'dev':
            ratings = self.load_ratings_dev()
        elif self.ratings_type == 'train':
            ratings = self.load_ratings_train()
        elif self.ratings_type == 'test':
            ratings = self.load_ratings_test()
        elif self.ratings_type == 'all':
            ratings = self.load_ratings_all()
        # FIXME remove if later
        if not (self.ratings_type == 'dev'): ratings.sample(frac=1)
        return ratings

    # Not tested
    # def build_generator_for_ratings_movie_meta(self, batch_size):
    #
    #     for pos in range(0, len(self.ratings), batch_size):
    #         cur_ratings = self.ratings.iloc[pos:pos + batch_size]
    #         cur_ratings = cur_ratings.merge(self.movie_features, on='movieId')
    #
    #         yield cur_ratings

    def build_generator_for_ratings_by_user_id(self, batch_size=10):
        """
        :param batch_size:
        :return: batch of ratings all with the same user_id
        """
        user_ids = self.ratings['userId'].unique()

        for pos in range(0, len(user_ids), batch_size):
            logging.info(
                'Position {pos} from {len} - {per}'.format(pos=pos, len=len(user_ids), per=(pos / len(user_ids))))
            cur_user_ids = user_ids[pos:pos + batch_size]
            cur_ratings = self.ratings[self.ratings['userId'].isin(cur_user_ids)]
            yield cur_ratings

    def build_generator_for_ratings_by_user_id_meta_movies(self, batch_size=10):
        batch_gen = self.build_generator_for_ratings_by_user_id(batch_size=batch_size)

        for ratings in batch_gen:
            cur_ratings = ratings
            cur_ratings = cur_ratings.merge(self.movie_features, on='movieId')
            yield cur_ratings

    def build_generator_for_ratings(self, ratings, batch_size):
        """
        :param ratings: given set of ratings
        :param batch_size: number of ratings processed simultaneously
        :return: a generator that return a X_feature df and y_ratings vector; X_features are the joined features of
        user_features + movie_features
        """
        for pos in range(0, len(ratings), batch_size):
            # logging.info(
            #     'Position {pos} from {len} - {per}'.format(pos=pos, len=len(ratings), per=(pos / len(ratings))))
            cur_ratings = ratings.iloc[pos:pos + batch_size]
            cur_ratings = cur_ratings.merge(self.user_features, left_on='userId', right_on='userId')
            cur_ratings = cur_ratings.merge(self.movie_features, on='movieId')
            X_chunk = cur_ratings.drop(
                ['userId', 'movieId', 'index', 'timestamp', 'rating', 'Unnamed: 0_x', 'Unnamed: 0_y',
                 'user_no_genres_listed'],
                axis=1)
            y_chunk = cur_ratings['rating']
            yield X_chunk, y_chunk

    def build_generator(self, batch_size):
        self.build_generator_for_ratings(self.ratings, batch_size)

    def drop_genome_na_ratings(self, ratings):
        """

        :param ratings:
        :return: ratings that have genome feature number 1228
        """
        movie_ids = self.movie_features[self.movie_features['1128'].notnull()].loc[:, 'movieId']
        ratings = ratings[ratings['movieId'].isin(movie_ids)]
        return ratings

# data_handler = DataHandlerFeatures(ratings_type='all')
# ratings = data_handler.load_ratings_dev()
# batch_gen = data_handler.build_generator_for_ratings(ratings=ratings, batch_size=5000)
#
# for X_chunk, y_chunk in batch_gen:
#     print(X_chunk.shape)
#     print(y_chunk.shape)







