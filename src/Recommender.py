from abc import ABC, abstractmethod

from src.DataHandler import DataHandler
from src.FeatureHandler import FeatureHandler


class Recommender(ABC):
    def __init__(self, data_handler, feature_handler, model):
        """ use ml-20m or ml-latest-small"""
        self.data_handler = data_handler
        self.feature_handler = feature_handler
        self.model = model

        self.X = None
        self.y = None

    @abstractmethod
    def fit(self,X,y):
        """
        Fit the recommender, so that recommend methods work. Update user_features etc.
        :param X:
        :param y:
        :return:
        """
    @abstractmethod
    def predict_rating_for_all_movies(self):
        """

        :return: DataFrame(userId, movieId, rating_predicted)
        """
    @abstractmethod
    def recommend_top_n(self):
        """

        :return: top n new recommendations  (without input items)
        """


