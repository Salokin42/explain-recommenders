# import pandas as pd
# from sklearn.model_selection import GroupKFold
#
# from DataHandler import DataHandler
# data_handler = DataHandler('ml-20m')
#
# ratings = data_handler.load_ratings_dev()
#
# X = ratings.loc[:,['movieId', 'timestamp', 'userId']]
# y = ratings.loc[:,'rating']
# groups = ratings.loc[:, 'userId']
#
# gkf = GroupKFold(n_splits=3)
#
# for train, test in gkf.split(X,y, groups=groups):
#     print(train, test)
#
#     #train_ratings, test_ratings = ratings.iloc[train], ratings.iloc[test]
#     # ratings.iloc[train].loc[:,'userId'].unique()


import numpy as np
from sklearn.model_selection import train_test_split
from sklearn import datasets
from sklearn import svm
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import GroupKFold, cross_val_score
from sklearn.metrics import mean_squared_error

from sklearn.metrics import accuracy_score

iris = datasets.load_iris()

# iris.data.shape, iris.target.shape
#
# X_train, X_test, y_train, y_test = train_test_split(
#     iris.data, iris.target, test_size=0.4, random_state=0)
#
# X_train.shape, y_train.shape
#
# X_test.shape, y_test.shape


clf = svm.SVC(kernel='linear', C=1)

# gkf = GroupKFold(n_splits=3).split(iris.data, iris.target, groups=iris.data[:,3])

X = iris.data
y = iris.target
groups = groups = iris.data[:, 3]

gkf = GroupKFold(n_splits=3)
scores = []

for train_index, test_index in gkf.split(X, y, groups=groups):
    X_train, X_test = X[train_index], X[test_index]
    y_train, y_test = y[train_index], y[test_index]

    clf.fit(X_train, y_train)

    y_pred = clf.predict(X_test)
    y_true= y_test
    score = mean_squared_error(y_true,y_pred)
    scores.append(score)
    print(score)



