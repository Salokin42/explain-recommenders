from abc import ABC, abstractmethod
import logging

import numpy as np
import pandas as pd
import lime
import lime.lime_tabular

from src.DataHandler import DataHandler

logger = logging.getLogger()
logger.setLevel(level=logging.DEBUG)


#### Rudimentarily designed classes

class Explainer(ABC):
    def __init__(self):
        self.feature_handler_fitted = None
        self.model_predict_function = None
        self.training_data = None
        self.feature_names = None
        self.n_explanation_features = None

        self.explainer = None

    @abstractmethod
    def fit(self, feature_handler_fitted, model_predict_function, training_data_representative, feature_names,
            n_explanation_features):
        self.feature_handler_fitted = feature_handler_fitted
        self.model_predict_function = model_predict_function
        self.training_data = training_data_representative
        self.feature_names = feature_names
        self.n_explanation_features = n_explanation_features
        # IMPLEMENT initialize Explainer here

    @abstractmethod
    def explain_X_feat(self, X):
        """

        :param X: df(userId, movieId)
        :return: lime.explanation
        """

    @abstractmethod
    def explain_X(self, X, n_explanation_features=10):
        """

        :param X: Singe X(userId, movieId)
        :param n_explanation_features:
        :return: lime explanation for X
        """

    def explain_Xs(self, Xs):
        """

        :param Xs: df with Multiple Xs(userId, movieID)
        :return: df with (userId, movieId, explanation)
        """
        explanations = Xs
        # explanations['explanation'] = np.nan
        for index, X in Xs.iterrows():
            X_ = X.loc[['userId', 'movieId']]
            explanation = self.explain_X(X_)
            explanations.loc[index, 'explanation'] = explanation
        return explanations


class RegressionExplainer(Explainer):
    def __init__(self):
        super().__init__()

    def fit(self, feature_handler_fitted, model_predict_function, training_data_representative,
            feature_names=None, n_explanation_features=5):
        """

        :param feature_handler_fitted:
        :param model_predict_function:
        :param training_data_representative: X_feat data that has enough different values for its features
        :return:
        """
        logging.info('Fitting {0}'.format(self.__class__))
        super().fit(feature_handler_fitted=feature_handler_fitted, model_predict_function=model_predict_function,
                    training_data_representative=training_data_representative, feature_names=feature_names,
                    n_explanation_features=n_explanation_features)

        if feature_names is None:
            self.feature_names = self.training_data.columns
        training_data_array = self.training_data.values
        self.explainer = lime.lime_tabular.LimeTabularExplainer(training_data_array, mode='regression',
                                                                feature_names=self.feature_names,
                                                                random_state=42,
                                                                discretize_continuous=True)
        return self


class InteractionRegressionExplainer(RegressionExplainer):
    """
    Explains the model using the whole interaction feature (movie_features * user_features).
    """

    def explain_X(self, X, n_explanation_features=10):
        logging.info('Generating Explanation for Instance {0}'.format(X))
        X_feat = self.feature_handler_fitted.transform(X)
        return self.explain_X_feat(X_feat)

    def explain_X_feat(self, X_feat):
        """
        Explain X in interaction feature format
        :param X_feat:
        :param n_explanation_features:
        :return:
        """
        explanation = self.explainer.explain_instance(X_feat.values, self.model_predict_function,
                                                      num_features=self.n_explanation_features)
        return explanation

    @staticmethod
    def build_training_data_representative(feature_handler_fitted, n, data_handler=DataHandler(), as_array=False):
        """
        Build training Data for the Explainer with n sample ratings
        :param feature_handler_fitted:
        :param n:
        :param data_handler:
        :param as_array:
        :return:
        """
        ratings = data_handler.load_ratings_dev()
        ratings = feature_handler_fitted.drop_ratings_where_movie_no_genome_tag(ratings,
                                                                                feature_handler_fitted.movie_features)
        X = ratings.loc[:, ['userId', 'movieId']]
        X = X.sample(frac=1)  # shuffle
        if not n or (n >= len(X)):
            n = len(X)
        X = X.iloc[0:n]
        X_feat = feature_handler_fitted.transform(X)
        if as_array:
            return X_feat.values
        else:
            return X_feat


class MovieFeatureRegressionExplainer(RegressionExplainer):
    """
    Explains the model varying the movieFeatures. Gives more or lessthe user_features * weight as explanations.
    Not so easy to explain to user.

    """

    def explain_X(self, X, n_explanation_features=10):
        logging.info('Generating Explanation for Instance {0}'.format(X))

        # true movie features for current X
        actual_movie_features = self.feature_handler_fitted.movie_features.loc[X['movieId']]

        # function with prefitted X
        def wrap_predict(movie_features_array):
            # Build interaction features out of inpur movie_features
            interaction_features = self.feature_handler_fitted.transform_given_movie_features(X=X,
                                                                            movie_features_array=movie_features_array)
            return self.model_predict_function(interaction_features)



        explanation = self.explainer.explain_instance(actual_movie_features.values, wrap_predict,
                                                      num_features=self.n_explanation_features)

        return explanation

    def explain_X_feat(self, X_feat):
        """
        Explain X in interaction feature format
        :param X_feat:
        :param n_explanation_features:
        :return:
        """
        pass

    @staticmethod
    def build_training_data_representative(feature_handler_fitted, n, data_handler=DataHandler(), as_array=False):
        """
        Build training Data for the Explainer with n sample ratings. Here different movie_features
        :param feature_handler_fitted:
        :param n: number of features to return
        :param data_handler:
        :param as_array: if return as array(for Lime) or as df
        :return: array or df of features to use as training data for lime
        """

        movie_features = feature_handler_fitted.movie_features.sample(frac=1).iloc[0:n]
        if as_array:
            return movie_features.values
        else:
            return movie_features

class UserFeatureRegressionExplainer(RegressionExplainer):
    """
    Explains the model varying the userFeatures. Give more or less the movie_features * weight as explanations.
    Can be interpreted as "the movie_features that correspond best with the actual user_features"
    """

    def explain_X(self, X, n_explanation_features=10):
        logging.info('Generating Explanation for Instance {0}'.format(X))

        # true movie features for current X
        actual_user_features = self.feature_handler_fitted.user_features.loc[X['userId']]

        # function with prefitted X
        def wrap_predict(user_features_array):
            # Build interaction features out of inpur movie_features
            interaction_features = self.feature_handler_fitted.transform_given_user_features(X=X,
                                                                            user_features_array=user_features_array)
            return self.model_predict_function(interaction_features)

        #FIXME Benutzt auch die wrap predict für das erste mal predict

        explanation = self.explainer.explain_instance(actual_user_features.values, wrap_predict,
                                                      num_features=self.n_explanation_features)

        return explanation

    def explain_X_feat(self, X_feat):
        """
        Explain X in interaction feature format
        :param X_feat:
        :param n_explanation_features:
        :return:
        """
        pass

    @staticmethod
    def build_training_data_representative(feature_handler_fitted, n, data_handler=DataHandler(), as_array=False):
        """
        Build training Data for the Explainer with n sample ratings. Here different movie_features
        :param feature_handler_fitted:
        :param n: number of features to return
        :param data_handler:
        :param as_array: if return as array(for Lime) or as df
        :return: array or df of features to use as training data for lime
        """
        ratings = data_handler.load_ratings_dev()
        ratings = feature_handler_fitted.drop_ratings_where_movie_no_genome_tag(ratings,
                                                                                feature_handler_fitted.movie_features)

        user_features = feature_handler_fitted.user_features.sample(frac=1).iloc[0:n]
        if as_array:
            return user_features.values
        else:
            return user_features
# _____________________________
#
# def explain_with_regression_weights(self, X_feat, features='interaction', n=10):
#     """
#
#     :param X: pandas df with X_feat
#     :param features: interaction = movie_features*user_features
#                     user = user_features
#                     movie = movie_features
#     :return: n top weighted features
#     """
#
#     weights = self.clf.coef_
#     weighted_features = X_feat * weights
#     highest_features = weighted_features.sort_values(ascending=False).iloc[0:n]
#     lowest_features = weighted_features.sort_values(ascending=True).iloc[0:n]
#
#     # if X would be whole df > not fully implemented yet
#     # hightest_features = weighted_features.apply((lambda x: x.sort_values().index), raw=False,
#     #                                             axis=1)  # does not work for single prediction ...
#
#     return highest_features, lowest_features, weighted_features

# explainer_reg = InteractionRegressionExplainer()
# explainer_reg.fit()
# X_feat = explainer_reg.X_feat
# X_feat = X_feat.iloc[0]
#
# highest_features, lowest_features = explainer_reg.explain_with_regression_weights(X_feat)

#
# weights = explainer_reg.clf.coef_
# weighted_features = X_feat * weights
