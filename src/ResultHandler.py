import datetime
import pandas as pd
import os
from pathlib import Path

from surprise import dump


class Result:
    def __init__(self, datasubset_name, model_class, rmse_avg, rmse_std, mae_avg, mae_std, validation_method,
                 parameters=None, notes=None,
                 timestamp=datetime.datetime.now(), model=None, ):
        self.dataset_name = datasubset_name
        self.model_class = model_class

        self.rmse_avg = rmse_avg
        self.rmse_std = rmse_std
        self.mae_avg = mae_avg
        self.mae_std = mae_std
        self.validation_method = validation_method

        self.parameters = parameters
        self.notes = notes
        self.timestamp = timestamp

        self.model = model


class ResultHandler:
    MODEL_FOLDER = Path(__file__).resolve().parent.parent / 'model'
    RESULT_COLUMNS = ['model_class', 'timestamp', 'dataset_name', 'rmse', 'mae', 'parameters', 'notes']

    def __init__(self, dataset):
        self.dataset = dataset
        # self.model_name = model_name

    def _build_model_save_path(self, result, subfolder='""'):
        return Path(
            self.MODEL_FOLDER / subfolder / (
                    result.timestamp.strftime('%Y-%m-%d_%H-%M-%S') + "_" + str(result.model_class)[8:-2]))

    def _build_results_path(self):
        return Path(self.MODEL_FOLDER / 'results.csv')

    # def _build_logfile_path(self):
    #     return Path(self.MODEL_FOLDER / 'logfile.csv')
    def save_result(self, result):
        # append to csv https://stackoverflow.com/questions/17530542/how-to-add-pandas-data-to-an-existing-csv-file
        result = pd.DataFrame(result.__dict__, index=[0])
        path = self._build_results_path()
        add_header = not path.exists()
        with open(path, 'a') as f:
            result.to_csv(f, header=add_header, index=False)

    def read_results(self):
        path = self._build_results_path()
        return pd.read_csv(path)

    def get_best_results(self, dataset=None):
        if dataset is None:
            dataset = self.dataset
        results = self.read_results()
        results = results.loc[results['dataset_name'] == dataset]
        indexes_best = results.groupby(['model_class'])['rmse_avg'].transform(max) == results['rmse_avg']
        best_results = results[indexes_best]
        return best_results

    def save_model(self, result):
        model_class = result.model_class
        if 'surprise.prediction_algorithms' in str(model_class):
            # make filename modelname+timestamp
            model_path = self._build_model_save_path(result, 'surprise')
            if not os.path.isdir(os.path.dirname(model_path)):
                os.makedirs(os.path.dirname(model_path))
            dump.dump(model_path, algo=result.model)
        else:
            print("Model is not defined in ResultHandler.save_model()")

    def load_model(self):
        # TODO implement
        pass


# mh = ResultHandler('ml-latest-small_try')

# mh.save_result('niko_test', '2012', 'testModel', '834', 'asdas', None, 'test')
# df = mh.read_results()
# print(df)
# mh.save_model('test', '25-4', 'test')
# data = ['niko_test', '2012', 'testModel', '834', 'asdas', None, None]

# dataset = 'ml-latest-small_try'
# results = mh.read_results()
# test = mh.get_best_results()


if __name__ == '__main__':
    solver = Solver()

    while True:
        a = int(input("a: "))
        b = int(input("b: "))
        c = int(input("c: "))
        result = solver.demo(a, b, c)
        print(result)
