# Copyright 2018 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# [START gae_python37_render_template]

import datetime
import random
import string
import time
import os
import json

from flask import Flask, render_template, request, redirect, url_for, session
from src.Recommender_Sklearn import KnnRecommender
from src.DataHandler import DataHandler

app = Flask(__name__)
app.secret_key = b'\x88\x82va\xa3W\x04\x9aa\xc6\xaa\xc0\xd7\x03\x145'
data_handler = DataHandler('ml-latest-small')


@app.route('/')
def root():
    # For the sake of example, use static information to inflate the template.
    # This will be replaced with real information in later steps.
    intro_movies = data_handler.flask_intro_movies()
    # TODO: Add images to Website
    # titles = intro_movies.loc[:, 'title'].tolist()
    # genres = intro_movies.loc[:, 'genres'].tolist()
    return render_template('index.html', intro_movies=intro_movies)


@app.route('/recommendation_calc', methods=['GET', 'POST'])
def get_recommendations():
    """ Calculate Recommendations"""
    recommender = recommender = KnnRecommender(
        # FIXME Fix Path
        os.path.join("data/raw/ml-latest-small", "movies.csv"),
        os.path.join("data/raw/ml-latest-small", "ratings.csv"),
        data_handler=DataHandler('ml-latest-small'))

    ratings = []
    userid_temp = ''.join(random.choices(string.digits, k=8))
    for movieId in request.form:
        form_rating = request.form[movieId]
        if form_rating is not "":
            rating = {}
            # TODO: Think about userID
            rating['userId'] = userid_temp
            rating['movieId'] = movieId
            rating['rating'] = form_rating
            rating['timestamp'] = time.time()
            ratings.append(rating)

    ratings = json.dumps(ratings)
    print(ratings)
    session['ratings'] = ratings

    # TODO: Store User input permanatly (DB, Logger, ...)

    recommendations_json = recommender.full_recommend_top_n(ratings)
    recommendations = json.loads(recommendations_json)
    session['recommendations'] = recommendations

    # TODO: For multiple recommendations

    # TODO: return redirect_to_rec_1
    return redirect(url_for('recommendation'))


@app.route('/recommendation')
def recommendation():
    # FIXME: If request from unipark all initialize the same session I'll have to solve it differently.
    # e.g. UserId and DB (SQL Lite)
    """
    Using sessions to store the recommendations. Will probably only work if
    :param n:
    :param test:
    :return:
    """
    recommendations = session['recommendations']
    if not recommendations:
        return render_template('no_recommendation_available.html')

    recommendation = recommendations.pop()
    session['recommendations'] = recommendations

    movie = data_handler.add_movie_information([recommendation['movieId']])
    title = movie['title'].to_string(index=False)

    return render_template('recommendation.html', rec=recommendation, title=title)


if __name__ == '__main__':
    # This is used when running locally only. When deploying to Google App
    # Engine, a webserver process such as Gunicorn will serve the app. This
    # can be configured by adding an `entrypoint` to app.yaml.
    # Flask's development server will automatically serve static files in
    # the "static" directory. See:
    # http://flask.pocoo.org/docs/1.0/quickstart/#static-files. Once deployed,
    # App Engine itself will serve those files as configured in app.yaml.
    app.run(host='127.0.0.1', port=8080, debug=True)
# [START gae_python37_render_template]
